PresentAR library and examples
Version 1.0.0

# About PresentAR 
`PresentAR` is a simplistic GUI & Rendering library for AR.
It is written in C++ and is complied with C++11 standard.
The goals of this library are to be simple.

## Version 1.0.0 (November 2014)

# Functionalities
## AR view rendering
- undistortion/ distortion

## Panoramic map rendering

## Scene graph
- simplisic scene graph handling

## GL based menu/button elements
- simplisitc based on OpenGL only

## Qt Quick, Qt QML support
- conventional classes for using Qt Quick & Qt QML


# Coding Guideline
`PresentAR` library and apps follows the C++ Style Guide from [googlecode.com](http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml)
- tools: used to check format & raise useful tips (~/tools/cpplint.py)
         (this tools is from the C++ Style Guide resource)
- code formating: check [clang-format](http://clang.llvm.org/docs/ClangFormat.html) tool.

# DEPENDENCIES
## Required dependencies for look3d core libraries
- `OpenGL` (2.1+)
- `FreeGlut` (2.8+)
- `GLEW` (for windows only)
