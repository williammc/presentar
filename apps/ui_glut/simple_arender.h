#ifndef APPS_UI_GLUT_SIMPLE_ARRENDER_H_
#define APPS_UI_GLUT_SIMPLE_ARRENDER_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "look3d/track/types.h"
#include "look3d/ui/gl_window.h"
#include "look3d/ui/gl_helpers.h"

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 480

namespace look3d {
class SimpleDrawable : public AbstractDrawable<ARRenderType> {
  virtual void Draw3D() {
    const float nbSteps = 200.0f;
    std::vector<float> vertices;
    std::vector<float> colors;
    for (int i = 0; i < nbSteps; ++i) {
      const float ratio = i/nbSteps;
      const float angle = 21.0f*ratio;
      const float c = cos(angle);
      const float s = sin(angle);
      const float r1 = 1.0f - 0.8f*ratio;
      const float r2 = 0.8f - 0.8f*ratio;
      const float alt = ratio - 0.5f;
      const float nor = 0.5f;
      const float up = sqrt(1.0f-nor*nor);
      vertices.push_back(r1*c);
      vertices.push_back(alt);
      vertices.push_back(r1*s);

      vertices.push_back(r2*c);
      vertices.push_back(alt+0.05f);
      vertices.push_back(r2*s);

      colors.push_back(1.0-ratio);
      colors.push_back(0.2f);
      colors.push_back(ratio);
      colors.push_back(0.5f);

      colors.push_back(1.0-ratio);
      colors.push_back(0.2f);
      colors.push_back(ratio);
      colors.push_back(0.5f);

    }

    Eigen::Matrix4f mvp = projection_matrix;
    Eigen::Matrix4f m = Eigen::Matrix4f::Identity();
    m(2, 3) = 2;
    mvp = mvp * m;
    std::vector<float> mvp_matrix = get_opengl_matrix<float>(mvp);

    glMatrixMode(GL_PROJECTION); glCheckError();
    glPushMatrix();
    glLoadIdentity(); glCheckError();
    glMatrixMode(GL_MODELVIEW); glCheckError();
    glPushMatrix();
    glLoadIdentity(); glCheckError();

    glMultMatrixf(&mvp_matrix.front());

    glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
    glEnableClientState(GL_COLOR_ARRAY); glCheckError();
    glVertexPointer(3, GL_FLOAT, 0, &vertices.front()); glCheckError();
    glColorPointer(4, GL_FLOAT, 0, &colors.front());
    glDrawArrays(GL_TRIANGLE_STRIP, 0, vertices.size() / 3);

    glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
    glDisableClientState(GL_COLOR_ARRAY); glCheckError();


    glMatrixMode(GL_MODELVIEW); glCheckError();
    glPopMatrix(); glCheckError();

    glMatrixMode(GL_PROJECTION); glCheckError();
    glPopMatrix(); glCheckError();
  }
};

class SimpleCallback : public GLWindowCallback {
 public:
  SimpleCallback() {
    Eigen::Matrix<double, 6, 1> v6_cam;
    v6_cam << FRAME_WIDTH, FRAME_HEIGHT,
                    FRAME_WIDTH/2, FRAME_HEIGHT/2, -0.3, 0.1;
    ar_render_.reset(new ARRenderType(FRAME_WIDTH, FRAME_HEIGHT,
                                      v6_cam[0], v6_cam[1], v6_cam[2],
                                      v6_cam[3], v6_cam[4], v6_cam[5]));
  }

  virtual void on_display(){
    // create synthetic image with 1 rectangle
    static cv::Mat bgr_img(FRAME_HEIGHT, FRAME_WIDTH, CV_8UC3, cv::Scalar(255));
    cv::rectangle(bgr_img,
                  cv::Rect(100, 100, 2*(FRAME_WIDTH/2-100), 2*(FRAME_HEIGHT/2-100)),
                  cv::Scalar(0,255,0));
    cv::Mat rgb_img;
    cv::cvtColor(bgr_img, rgb_img, CV_BGR2RGB);
    ar_render_->set_frame_pixels(rgb_img.cols, rgb_img.rows, 3,
                                 (unsigned char*)rgb_img.data);
    ar_render_->Render();
  }
  virtual void keyboard (unsigned char c_key, int x, int y) {
    c_key_pressed = c_key;
    switch (c_key) {
    case 'r':
      break;
    }
    std::cout << "pressed:" << c_key << std::endl;
  }

  unsigned char c_key_pressed;
};
}  // namespace look3d
#endif  // APPS_UI_GLUT_SIMPLE_ARRENDER_H_
