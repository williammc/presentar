
#include "simple_arender.h"

using namespace look3d;
using namespace cv;

int main(int argc, char** argv) {
  SimpleDrawable simple_drawable;

  SimpleCallback simple_callback;
  simple_callback.get_ar_render()->add_drawable(&simple_drawable);
  GLWindow gl_window(&simple_callback);
  gl_window.init("Simple ARender", FRAME_WIDTH, FRAME_HEIGHT, argc, argv);
  gl_window.start_loop();
}


