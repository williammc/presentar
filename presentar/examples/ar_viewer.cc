#include "presentar/core/gl_window.h"
#include "presentar/core/ar_node.h"
#include "presentar/util/triangle_node.h"
#include "presentar/util/png_io.h"

using namespace present;

static Eigen::VectorXf cam_params;

#if 0
typedef ARNodePoliCam CameraType;
typedef ARNodePoliCam ARNodeType;
typedef ARNodePoliCamPtr ARNodePtr;

#else
typedef ARNodeAtanCam CameraType;
typedef ARNodeAtanCam ARNodeType;
typedef ARNodeAtanCamPtr ARNodePtr;
#endif

class ARGLWCallback : public GLWCallback {
public:
  ARGLWCallback() {
    viewer_ = ARNodePtr(new CameraType(cam_params, 0.0f, 1.0f));
  }

  virtual void OnDisplay() {
    static bitmap_t bitmap;
    static bool first = true;
    if (first) {
      first = false;
      std::string filename = std::string(PresentAR_ROOT)
        + "/presentar/examples/data/firefly_checkerbooard.png";
      printf("Read png file from %s\n", filename.c_str());
      read_png_file(filename.c_str(), bitmap);

      write_png_file(bitmap, (filename + "test.png").c_str());
    }
    printf("SetFramePixels to ARNode\n");
    dynamic_cast<ARNodeType*>(viewer_.get())->SetFramePixels(cam_params[0],
                                                                cam_params[1],
                                                                bitmap.bit_depth*bitmap.pixel_size/8,
                                                                &bitmap.pixels.front(),
                                                                GL_RGBA);

    viewer_->Update();
  }

  virtual void Keyboard(unsigned char key, int x, int y)  {
    auto arnode = dynamic_cast<ARNodeType*>(viewer_.get());
    switch (key) {
    case 'n':
      arnode->SetNormalRender();
      break;
    case 'd':
      arnode->SetDistortionRender();
      break;
    case 'u':
      arnode->SetUndistortionRender();
      break;
    }
  }

};

int main(int argc, char** argv) {
#if 0
  cam_params.resize(8);
  cam_params << 640, 480,  // width, height
                606.9783850066542, 608.5636160810556,  // fx, fy
                329.0883849012152, 240.9498031221482,  // cx, cy
                -0.367846464948389, 0.1578975499985;  // d1, d2
#else 
  cam_params.resize(7);
  cam_params << 640, 480,  // width, height
    606.9783850066542, 608.5636160810556,  // fx, fy
    329.0883849012152, 240.9498031221482,  // cx, cy
    0.961982;  // w
#endif
  std::shared_ptr<ARGLWCallback> scb(new ARGLWCallback());
  scb->viewer()->AddChild(TriangleNodePtr(new TriangleNode()));

  GLWindow gl_window(scb.get());
  gl_window.Init("Simple Window with ARNode and TriangleNode",
                 640, 480, argc, argv);
  gl_window.StartLoop();
}


