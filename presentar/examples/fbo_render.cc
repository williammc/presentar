#include "presentar/core/gl_window.h"
#include "presentar/core/gl_window_menu.h"
#include "presentar/core/fbo_node.h"
#include "presentar/util/orientation_node.h"
#include "presentar/util/triangle_node.h"

using namespace present;

class FboGLWCallback : public SimpleGLWCallback {
public:
  FboGLWCallback() : SimpleGLWCallback() {
    fbo_.reset(new FboNode(Vec2f(600, 600)));
    fbo_->AddChild(OrientationNodePtr(new OrientationNode()));
    fbo_->AddChild(TriangleNodePtr(new TriangleNode()));
    viewer_->AddChild(fbo_);
  }

  virtual void OnDisplay() {
    viewer_->Update();
    auto v = viewer_->WindowSize();
    fbo_->MapTextureToFBO(Rect{ 0, 0, v[0], v[1] }, 0);
  }

  virtual void Keyboard(unsigned char key, int x, int y) {
    switch (key) {
    case 'c':
      fbo_->SaveSnapshot("fbo_snapshot");
      break;
    }
  }

  FboNodePtr fbo_;
};

int main(int argc, char** argv) {
  
  FboGLWCallback scb;
  GLWindowMenuPtr menu1(new GLWindowMenu("Menu1", "Menu1"));
  menu1->AddSubMenu(GLWindowMenu::MenuItemType::Button, "Root  Sub1 Sub1 Root");
  menu1->AddSubMenu(GLWindowMenu::MenuItemType::Button, "Root Sub2 Sub2 Root");
  GLWindowMenuPtr menu2(new GLWindowMenu("Menu2", "Menu2"));

  GLWindow gl_window(&scb);
  gl_window.Init("Simple Window with FboNode and TriangleNode",
                 640, 480, argc, argv);
  gl_window.AddMenu(menu1);
  gl_window.AddMenu(menu2);
  gl_window.StartLoop();
}


