#include <iostream>

#ifndef ANDROID
#include "config.h"
#else
#define CONFIG_FILE ""
#endif
#include "panoramic_tracker_glut_callback.h"

using namespace std;
using namespace look3d;

static PanoramicTrackerGlutCallback* spano_callback = 0;
static int gl_window = 0;
static int menu_value = 0;

void menu_callback(int num){
  if (num == 0) {
    glutDestroyWindow(gl_window);
    exit(0);
  } else {
    switch (num) {
    case 1: LOG("Render Teapot"); break;
    case 2: LOG("Render Torus"); break;
    case 3: LOG("Save Map"); break;
    case 4: spano_callback->RestartTracker(); break;
    }

    menu_value = num;
  }
  glutPostRedisplay();
}

void create_menu(void){
  glutCreateMenu(menu_callback); glCheckError();
  glutAddMenuEntry("Restart Tracker", 4); glCheckError();
  glutAddMenuEntry("Save Map", 3); glCheckError();
  glutAddMenuEntry("Torus", 2); glCheckError();
  glutAddMenuEntry("Teapot", 1); glCheckError();
  glutAddMenuEntry("Quit", 0); glCheckError();
  glutAttachMenu(GLUT_RIGHT_BUTTON); glCheckError();
}

int main (int argc, char * argv[]) {
  string configfile = (argc > 1) ? argv[1] : CONFIG_FILE;
  cout << " loading config file from: " << configfile << endl;

  boost::property_tree::ptree config_pt;
  if (FileExists(configfile)) 
      boost::property_tree::ini_parser::read_ini(configfile, config_pt);

  PanoramicTrackerGlutCallback pano_callback;
  spano_callback = &pano_callback;
  if (!pano_callback.Configure(configfile)) {
    printf("ERROR: Fail to Configure %s\n", configfile.c_str());
    return -1;
  }
  
  ARDrawable<PanoramicTrackerGlutCallback> ar_drawable(&pano_callback);
  pano_callback.get_ar_render()->add_drawable(&ar_drawable);

  GLWindow window(&pano_callback);
  window.init("Panoramic Tracker (GLUT based)",
                 640, 480, argc, argv);
  create_menu();
  gl_window = window.properties.glut_window_id;
  window.start_loop();
}

