#version 400
uniform mat4 mvp_matrix;
uniform vec4 uniform_color;
uniform float use_uniform_color;
in vec3 vertex_position;
in vec3 vertex_normal;
in vec4 vertex_color;

out vec3 v_normal;
out vec4 v_color;
void main() {
  gl_Position = mvp_matrix * vec4(vertex_position, 1.0);
  v_normal = vertex_normal;
  if (use_uniform_color == 1.0) {
    v_color = uniform_color;
  } else {
    v_color = vertex_color;
  }
};
