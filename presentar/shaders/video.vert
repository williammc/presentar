#version 400
in vec3 vertex_position;
in vec2 vertex_texcoord;

out vec2 out_texcoord;
void main() {
  out_texcoord = vertex_texcoord;
  gl_Position = vec4(vertex_position, 1.0);
}
