#version 400
in vec2 out_texcoord;
uniform sampler2D video_texture;
out vec4 out_color;

void main() {
  out_color = texture2D(video_texture, out_texcoord);
}


