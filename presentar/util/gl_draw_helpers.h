// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <array>
#include "presentar/core/opengl.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/presentar_api.h"

namespace present {

inline void glColor(const std::array<float, 4> &color) {
  glColor4f(color[0], color[1], color[2], color[3]);
}

// Draw a Unit Cube
inline void glDrawUnitCube(const std::array<float, 4> &color) {
  std::array<float, 72> cub_pos = {
    1.0f, 1.0f, -1.0f, // Top Right Of The Quad (Top)
    -1.0f, 1.0f, -1.0,  // Top Left Of The Quad (Top)
    -1.0f, 1.0f, 1.0f, // Bottom Left Of The Quad (Top)
    1.0f, 1.0f, 1.0f,// Bottom Right Of The Quad (Top)

    1.0f, -1.0f, 1.0f, // Top Right Of The Quad (Bottom)
    -1.0f, -1.0f, 1.0,  // Top Left Of The Quad (Bottom)
    -1.0f, -1.0f, -1.,  // Bottom Left Of The Quad (Bottom)
    1.0f, -1.0f, -1.0,  // Bottom Right Of The Quad (Bottom)

    1.0f, 1.0f, 1.0f,// Top Right Of The Quad (Front)
    1.0f, -1.0f, 1.0f, // Bottom Right Of The Quad (Front)
    -1.0f, -1.0f, 1.0,  // Bottom Left Of The Quad (Front)
    -1.0f, 1.0f, 1.0f, // Top Left Of The Quad (Front)

    1.0f, -1.0f, -1.0,  // Bottom Left Of The Quad (Back)
    1.0f, 1.0f, -1.0f, // Top Left Of The Quad (Back)
    -1.0f, 1.0f, -1.0,  // Top Right Of The Quad (Back)
    -1.0f, -1.0f, -1.,  // Bottom Right Of The Quad (Back)

    -1.0f, 1.0f, 1.0f, // Top Right Of The Quad (Left)
    -1.0f, 1.0f, -1.0,  // Top Left Of The Quad (Left)
    -1.0f, -1.0f, -1.,  // Bottom Left Of The Quad (Left)
    -1.0f, -1.0f, 1.0,  // Bottom Right Of The Quad (Left)

    1.0f, 1.0f, -1.0f, // Top Right Of The Quad (Right)
    1.0f, 1.0f, 1.0f,// Top Left Of The Quad (Right)
    1.0f, -1.0f, 1.0f, // Bottom Left Of The Quad (Right)
    1.0f, -1.0f, -1.0,  // Bottom Right Of The Quad (Right)
  };

  glColor(color);
  glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
  glVertexPointer(3, GL_FLOAT, 0, &cub_pos.front()); glCheckError();
  glDrawArrays(GL_QUADS, 0, 24); glCheckError();
  glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
}

// Draw red cube, green cube, and blue cube as (XYZ axises).
inline void glDrawAugmentation(const std::array<float, 16>& pose,
                                const float scale) {
  // draw nice boxes
  // glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  glMultMatrixf(&pose.front());
  glTranslated(0, 0, 0);

  {
    const float alpha = 1;

    // X-axis
    glPushMatrix();
    glScaled(scale, scale*0.1, scale*0.1);
    glTranslated(0, 0, 1);
    glDrawUnitCube(std::array<float, 4>{0.8, 0, 0, alpha});
    glPopMatrix();

    // Y-axis
    glPushMatrix();
    glScaled(scale*0.1, scale, scale*0.1);
    glTranslated(0, 0, 1);
    glDrawUnitCube(std::array<float, 4>{0.0, 0.8, 0, alpha});
    glPopMatrix();

    // Z-axis
    glPushMatrix();
    glScaled(scale*0.1, scale*0.1, scale);
    glTranslated(0, 0, 1);
    glDrawUnitCube(std::array<float, 4>{0, 0, 0.9, alpha});
    glPopMatrix();
  }

  glPopMatrix();
  // glDisable(GL_DEPTH_TEST);
}

// Draw a Unit Triangle
inline void glDrawTriangle(const std::array<float, 9>& pos,
                    const std::array<float, 12>& colors) {
  //  glEnable(GL_DEPTH_TEST);
  glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
  glEnableClientState(GL_COLOR_ARRAY); glCheckError();
  glVertexPointer(3, GL_FLOAT, 0, &pos.front()); glCheckError();
  glColorPointer(4, GL_FLOAT, 0, &colors.front()); glCheckError();

  glDrawArrays(GL_TRIANGLES, 0, 3); glCheckError();

  glDisableClientState(GL_COLOR_ARRAY); glCheckError();
  glDisableClientState(GL_VERTEX_ARRAY); glCheckError();

  //  glDisable(GL_DEPTH_TEST);
}

inline void glDrawNiceArrow(GLUquadric* qobj,
                      const std::array<float, 4> color,
                      const float cy_base,
                      const float cy_height,
                      const float slices = 10,
                      const float stacks = 10) {
  // cylinder
  glColor4f(color[0] * 0.5, color[1] * 0.5, color[2] * 0.5, color[3]);
  gluCylinder(qobj, cy_base, cy_base, cy_height, 2 * slices, stacks);

  // top-cone
  glPushMatrix();
  glTranslatef(0.0f, 0.0f, cy_height);
  glColor(color);
  gluCylinder(qobj, cy_base*2.0f, 0.0f, 1.0f - cy_height, slices, stacks);
  glPopMatrix();
}

inline void glDrawText(const std::string& text) {
  for (size_t i = 0; i < text.length(); ++i)
    glutStrokeCharacter(GLUT_STROKE_ROMAN, text[i]);
}
}  // namespace present