// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <fstream>
#include "presentar/core/datatypes.h"

namespace presentar {
/// read whole content of a file into a string
static std::string Read(const std::string& filename) {
  std::ifstream ifs(filename);
  if (ifs.failed()) return std::string();

  return std::string((std::istreambuf_iterator<char>(ifs)),
                     std::istreambuf_iterator<char>());
}

static Mat4f Rotation180x() {
  Mat4f rot_180x; // rotate around X-axis 180degree (vision to OpenGL)
  rot_180x << 1.0f, 0.0f, 0.0f, 0.0f,
              0.0f, -1.0f, 0.0f, 0.0f,
              0.0f, 0.0f, -1.0f, 0.0f,
              0.0f, 0.0f, 0.0f, 1.0f;
  return rot_180x;
}

}  // namespace presentar