// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "presentar/core/present_node.h"
#include "presentar/presentar_api.h"

namespace present {

struct TextNode : public PresentNode {
  TextNode(std::string text, uint32_t id = 0, PresentNode* parent = nullptr) 
  : text_(text), PresentNode(id, parent) {}
  
  void Init() {}

  void Render() {
    if (gl_dis_) {
      glCallList(gl_dis_);
    } else {
      gl_dis_ = glGenLists(1);
      glNewList(gl_dis_, GL_COMPILE);
     
      for (size_t i = 0; i < text_.length(); ++i)
        glutStrokeCharacter(GLUT_STROKE_ROMAN, text_[i]);
     
      glEndList();
    }
  }

 protected:
  std::string text_;
  GLuint gl_dis_;
};

}  // namespace present
