// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "presentar/core/opengl.h"
#include "presentar/core/present_node.h"
#include "presentar/util/gl_draw_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
FORWARD_DECLARE_CLASS_SHARED_PTR(AxisNode);
FORWARD_DECLARE_CLASS_SHARED_PTR(TextNode);

class PRESENTAR_API AxisNode : public PresentNode {
  AxisNode(const AxisNode&) = delete;
  AxisNode(const float scale = 1.0f, uint32_t id = 0, 
           PresentNode *parent = nullptr)
    : scale_(scale), PresentNode(id, parent) {
    all_dis_ = 0;
  }

  ~AxisNode() {
    if (all_dis_) glDeleteLists(all_dis_, 1);
  }

  void Init() {
    PresentNode::Init();

    textx_node_.reset(new TextNode("X"));
    texty_node_.reset(new TextNode("Y"));
    textz_node_.reset(new TextNode("Z"));
    // generate vertices for x-plane, y-plane, & z-plane
    const int nsubdiv = 360;
    plane_cir_vers_.resize(nsubdiv * 9);
    for (int i = 0; i < nsubdiv; ++i) {
      const float t = float(i)*360.0f / (nsubdiv - 1);
      const float angle = t*M_PI / 180.0f;
      const float d1 = std::cos(angle) * scale_ * 2;
      const float d2 = std::sin(angle) * scale_ * 2;
      plane_cir_vers_[3 * i] = 0.0f;
      plane_cir_vers_[3 * i + 1] = d1;
      plane_cir_vers_[3 * i + 2] = d2;

      plane_cir_vers_[nsubdiv * 3 + 3 * i] = d1;
      plane_cir_vers_[nsubdiv * 3 + 3 * i + 1] = 0.0f;
      plane_cir_vers_[nsubdiv * 3 + 3 * i + 2] = d2;

      plane_cir_vers_[nsubdiv * 6 + 3 * i] = d1;
      plane_cir_vers_[nsubdiv * 6 + 3 * i + 1] = d2;
      plane_cir_vers_[nsubdiv * 6 + 3 * i + 2] = 0.0f;
    }

  }

  void Render() {

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    auto pose = to_opengl(mvp_matrix_);
    glMultMatrixf(&pose.front());

    const float cy_base = 0.025;
    const float cy_height = 0.9;
    const float slices = 50;
    const float stacks = 50;
    const float font_scale = 0.1f;

    glEnable(GL_DEPTH_TEST);

    if (all_dis_) {
      glCallList(all_dis_);
    } else {
      all_dis_ = glGenLists(1);
      glNewList(all_dis_, GL_COMPILE);
      auto quad = gluNewQuadric();

      const float alpha = 1.0f;

      auto draw_text = [&](TextNodePtr text_node) {
        glPushMatrix();
        glTranslatef(0.0f, -font_scale / 2.0f, 1.1f);
        glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
        glScalef(font_scale, font_scale, font_scale);
        text_node->Render();
        glPopMatrix();
      };

      // x-axis
      glPushMatrix();
      glScalef(scale_, scale_, scale_);
      glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
      glDrawNiceArrow(quad, std::array<float, 4>{1.0, 0.0, 0.0, alpha},
                      cy_base, cy_height, slices, stacks);
      draw_text(textx_node_);
      glPopMatrix();

      // y-axis
      glPushMatrix();
      glScalef(scale_, scale_, scale_);
      glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
      glDrawNiceArrow(quad, std::array<float, 4>{0.0, 1.0, 0.0, alpha},
                      cy_base, cy_height, slices, stacks);
      draw_text(texty_node_);
      glPopMatrix();

      // z-axis
      glPushMatrix();
      glScalef(scale_, scale_, scale_);
      glDrawNiceArrow(quad, std::array<float, 4>{0.0, 0.0, 1.0, alpha},
                      cy_base, cy_height, slices, stacks);
      draw_text(textz_node_);
      glPopMatrix();

      // sphere at the origin
      glPushMatrix();
      glColor4f(0.5, 0.5, 0.5, 1.0);
      glScalef(scale_, scale_, scale_);
      gluSphere(quad, cy_base, slices, stacks);
      glPopMatrix();

      gluDeleteQuadric(quad);

      glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
      glLineWidth(2.0f);
      glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
      glVertexPointer(3, GL_FLOAT, 0, &plane_cir_vers_.front()); glCheckError();
      glDrawArrays(GL_LINE_STRIP, 0, plane_cir_vers_.size() / 3); glCheckError();
      glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
      glEndList();
    }

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
  }

  void Sync() {
    ComputeBBox();
  }

  void ComputeBBox() {
    bbox_.min_x = std::min(bbox_.min_x, -scale_);
    bbox_.min_y = std::min(bbox_.min_y, -scale_);
    bbox_.min_z = std::min(bbox_.min_z, -scale_);

    bbox_.max_x = std::max(bbox_.max_x, scale_);
    bbox_.max_y = std::max(bbox_.max_y, scale_);
    bbox_.max_z = std::max(bbox_.max_z, scale_);
  }

 protected:
  // Properties ----------------------------------------------------------------
  float scale_;
  TextNodePtr textx_node_, texty_node_, textz_node_;
  std::vector<float> plane_cir_vers_;  // 3-circles on x-plane, y-plane, z-plane

  GLuint all_dis_;
};
}  // namespace present