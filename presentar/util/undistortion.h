// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <vector>
#include "presentar/core/datatypes.h"
#include "presentar/core/opengl.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/presentar_api.h"

namespace present {

struct Frustum {
 float left, right, top, bottom;
};

struct UndistortionInfo {
  struct TextureMappingCoords {
    unsigned sample_steps;       ///< number of sample steps
    std::vector<float> vertices;   ///< 2D vertex coordinates
    std::vector<float> texcoords;  ///< normalized 2D texture coordinates [0, 1]
  };

  Vec2f video_size;         ///< (width, height) of video frame
  Vec2f undist_video_size;  ///< size that wrap
  
  TextureMappingCoords d2u_map;  ///< distorted to undistorted
  TextureMappingCoords u2d_map;  ///< undistored to distorted
  Frustum frustum; ///< OpenGL coordinates frustum matrix for undistortion rendering
};

/// Calculate data for undistortion and distortion mapping for video frame
template<typename CameraModel>
inline void CalcUndistortionInfo(const CameraModel& cam,
                                 UndistortionInfo& undist, 
                                 const int sample_steps = 24,
                                 bool need_opengl_convert = true) {
  // Step1 : figuring anchor corners of undistored video frame (tl, br)
  undist.video_size = cam.ImageSize();
    // work out FrameBuffer size to cover undistorted video frame
  const Vec2f tl = cam.LinearProject(cam.UnProject(Vec2f(0.0, 0.0)));
  const Vec2f br = cam.LinearProject(cam.UnProject(
                Vec2f(undist.video_size[0], undist.video_size[1])));
  Vec2f offset =
      br - Vec2f(undist.video_size[0], undist.video_size[1]);
  if (offset.norm() < tl.norm()) {
    offset = -tl;
  }
  float max_ratio = std::max(
      (undist.video_size[0] + offset[0]) / undist.video_size[0],
      (undist.video_size[1] + offset[1])/undist.video_size[1]);
  max_ratio *= 1.2;  // larger to surely cover undistorted video frame texture

  undist.undist_video_size[0] = (int)undist.video_size[0]*max_ratio;
  undist.undist_video_size[1] = (int)undist.video_size[1]*max_ratio;

  // reserving memory to speedup
  undist.d2u_map.sample_steps = sample_steps;
  undist.d2u_map.vertices.reserve(sample_steps*sample_steps);
  undist.d2u_map.texcoords.reserve(sample_steps*sample_steps);
  undist.u2d_map.vertices.reserve(sample_steps*sample_steps);
  undist.u2d_map.texcoords.reserve(sample_steps*sample_steps);

  // Step2 : calculate undistortion & distortion mapping data
  offset = 0.5f*(undist.undist_video_size - undist.video_size);
  Vec2f v2_distorted, v2_undistorted;
  for (int y = 0; y < sample_steps; ++y) {
    for (int x = 0; x < sample_steps; ++x) {
      v2_distorted[0] = static_cast<float>(x*undist.video_size[0])/(sample_steps-1);
      v2_distorted[1] = static_cast<float>(y*undist.video_size[1])/(sample_steps-1);
      if (x == 0 || y == 0) {
        v2_distorted[0] = v2_distorted[0] - 3;
        v2_distorted[1] = v2_distorted[1] - 3;
      } else if (x == sample_steps-1 || y == sample_steps-1) {
        v2_distorted[0] = v2_distorted[0] + 3;
        v2_distorted[1] = v2_distorted[1] + 3;
      }

      v2_undistorted = cam.LinearProject(cam.UnProject(v2_distorted));

      // video frame (vision coordinates) to FBO (OpenGL coordinates)
      undist.d2u_map.texcoords.push_back(v2_distorted[0] / undist.video_size[0]);
      undist.d2u_map.texcoords.push_back(v2_distorted[1] / undist.video_size[1]);
      undist.d2u_map.vertices.push_back((v2_undistorted[0]+offset[0]) / undist.undist_video_size[0]);
      undist.d2u_map.vertices.push_back((v2_undistorted[1]+offset[1]) / undist.undist_video_size[1]);
      if (need_opengl_convert)  // convert to OpenGL coordinates system
        undist.d2u_map.vertices.back() = 1.0f - undist.d2u_map.vertices.back();

      // FBO to system's default FBO
      undist.u2d_map.texcoords.push_back((v2_undistorted[0] + offset[0]) / undist.undist_video_size[0]);
      undist.u2d_map.texcoords.push_back((v2_undistorted[1] + offset[1]) / undist.undist_video_size[1]);
      if (need_opengl_convert)  // convert to OpenGL coordinates system
        undist.u2d_map.texcoords.back() = 1.0f - undist.u2d_map.texcoords.back();

      undist.u2d_map.vertices.push_back(v2_distorted[0] / undist.video_size[0]);
      undist.u2d_map.vertices.push_back(v2_distorted[1] / undist.video_size[1]);
    }
  }
  undist.u2d_map.sample_steps = sample_steps;

  // Step 3: calculate frustum matrix
  const Vec2f v2 = offset;
  // note: this is in Vision coordinates (start top-left)
  const Vec2f tl1 = cam.LinearUnProject(Vec2f(-v2[0], -v2[1]));
  const Vec2f br1 = cam.LinearUnProject(Vec2f(undist.video_size[0] + v2[0],
                                             undist.video_size[1] + v2[1]));

  undist.frustum.left = float(tl1[0]);
  undist.frustum.right = float(br1[0]);
  undist.frustum.top = -float(tl1[1]);  // transform to OpenGL coordinates.
  undist.frustum.bottom = -float(br1[1]);
}

/// Prepare texture mapping steps
inline void glBindMapTextureData(GLuint texture,
                            const float* vertices,
                            const float* texcoords) {
  // swap vertial axis glOrtho(left, right, bottom, top)
  // => transforms OpenCV coordinates to OpenGL coordinates in 2D drawing
  glMatrixMode(GL_PROJECTION); glCheckError();
  glPushMatrix();
  glLoadIdentity(); glCheckError();
  // l, r, b, t, near, far
  glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);  // OpenGL coordinates (start at bottom-left)

  glMatrixMode(GL_MODELVIEW); glCheckError();
  glPushMatrix();
  glLoadIdentity(); glCheckError();

  glBindTexture(GL_TEXTURE_2D, texture); glCheckError();

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); glCheckError();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); glCheckError();


  glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
  glEnableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();
  glVertexPointer(2, GL_FLOAT, 0, vertices); glCheckError();
  glTexCoordPointer(2, GL_FLOAT, 0, texcoords); glCheckError();
}

/// Unbind steps 
inline void glUnbindMapTextureData() {
  glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
  glDisableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();

  glMatrixMode(GL_MODELVIEW); glCheckError();
  glPopMatrix(); glCheckError();

  glMatrixMode(GL_PROJECTION); glCheckError();
  glPopMatrix(); glCheckError();
}

// Undistorting a video frame texture
inline void glUndistortTexture(const GLuint frametex,
                        const UndistortionInfo& undist) {
  glBindMapTextureData(frametex,
                       &undist.d2u_map.vertices.front(),
                       &undist.d2u_map.texcoords.front()); glCheckError();
  for (unsigned y = 0; y < unsigned(undist.d2u_map.sample_steps - 1); ++y) {
    std::vector<unsigned> elements;
    for (unsigned x = 0; x < unsigned(undist.d2u_map.sample_steps); ++x) {
      elements.push_back(x + (y + 1)*undist.d2u_map.sample_steps);
      elements.push_back(x + (y)*undist.d2u_map.sample_steps);
    }
    glDrawElements(GL_TRIANGLE_STRIP, elements.size(), GL_UNSIGNED_INT, &elements.front()); glCheckError();
  }
  glUnbindMapTextureData(); glCheckError();
}

// Distortion mapping: from the undistorted texture (from @glUndistortTexture) 
inline void glDistortTexture(const GLuint frametex,
                      const UndistortionInfo& undist) {
  glBindMapTextureData(frametex,
                       &undist.u2d_map.vertices.front(),
                       &undist.u2d_map.texcoords.front());
  for (unsigned y = 0; y < unsigned(undist.u2d_map.sample_steps - 1); ++y) {
    std::vector<unsigned> elements;
    for (unsigned x = 0; x < unsigned(undist.u2d_map.sample_steps); ++x) {
      elements.push_back(x + (y + 1)*undist.u2d_map.sample_steps);
      elements.push_back(x + (y)*undist.u2d_map.sample_steps);
    }
    glDrawElements(GL_TRIANGLE_STRIP, elements.size(), GL_UNSIGNED_INT, &elements.front()); glCheckError();
  }
  glUnbindMapTextureData();
}

// OpenCV alike approach ======================================================
/// Undistortion data for inverse mapping (kinda like OpenCV)
/// similar to cv::initUndistortRectifyMap(*m_undist_remap_x, *m_undist_remap_y)
struct OcvUndistortionInfo {

  Vec2f video_size;         ///< (width, height) of video frame
  std::vector<float> u2d_map_x;  ///< mapping from undist. img. to dist. img.
  std::vector<float> u2d_map_y;

  Frustum frustum;
};

/// undistortion warping using opencv
template<typename Camera>
inline void CalcOcvUndistortionInfo(const Camera& cam,
                                    OcvUndistortionInfo& undist) {
  undist.video_size = cam.ImageSize();
  undist.u2d_map_x.reserve(undist.video_size[0]*undist.video_size[1]);
  undist.u2d_map_y.reserve(undist.video_size[0]*undist.video_size[1]);

  Vec2f dist, v2_undist;
  for (int y = 0; y < undist.video_size[1]; ++y) {
    for (int x = 0; x < undist.video_size[0]; ++x) {
      v2_undist[0] = x;
      v2_undist[1] = y;

      dist = cam.Project(cam.UnProjectLinear(v2_undist));
      undist.u2d_map_x.push_back(dist[0]);
      undist.u2d_map_x.push_back(dist[1]);
    }
  }

  // calculate frustum matrix
  const Vec2f v2 = offset;
  // note: this is in Vision coordinates (start top-left)
  const Vec2f tl = cam.UnProjectLinear(Vec2f(0, 0));
  const Vec2f br = cam.UnProjectLinear(Vec2f(undist.video_size[0],
                                             undist.video_size[1]));

  undist.frustum.left = float(tl[0]);
  undist.frustum.right = float(br[0]);
  undist.frustum.top = -float(tl[1]);  // transform to OpenGL coordinates.
  undist.frustum.bottom = -float(br[1]);
}

}  // namespace present