#pragma once
#include "presentar/core/opengl.h"
#include "presentar/core/present_node.h"
#include "presentar/util/gl_draw_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
FORWARD_DECLARE_CLASS_SHARED_PTR(AnnulusNode);

/// For drawing Annulus in 3D space 
/// @ref: http://en.wikipedia.org/wiki/Annulus_%28mathematics%29
class PRESENTAR_API AnnulusNode : public PresentNode {
 public:
  AnnulusNode() = delete;
  AnnulusNode(const AnnulusNode&) = delete;
  /// @inner_radius must be in range [0, 1]
  AnnulusNode(const float inner_radius,
              const std::array<float, 4>& border_color,
              const std::array<float, 4>& face_color,
              const uint16_t subdivision = 255,
              uint32_t id = 0,
              PresentNode *parent = nullptr)
  : inner_radius_(inner_radius),
      border_color_(border_color),
      face_color_(face_color),
      subdivision_(subdivision),
      PresentNode(id, parent) {
    // workout 3D geometry of the Annulus
    inner_vertices_.resize(3*subdivision_);
    outer_vertices_.resize(3*subdivision_);
    for (int i = 0; i < subdivision_; ++i) {
      const float t = float(i)*360.0f/(subdivision_-1);
      const float angle = t*M_PI/180.0f;
      const float x = std::cos(angle);
      const float y = std::sin(angle);
      inner_vertices_[3*i] = x*inner_radius_;
      inner_vertices_[3*i + 1] = y*inner_radius_;
      inner_vertices_[3*i + 2] = 0.0f;

      outer_vertices_[3*i] = x;
      outer_vertices_[3*i + 1] = y;
      outer_vertices_[3*i + 2] = 0.0f;
    }

    triangle_strips_vertices_.resize(6*subdivision_);
    for (int i = 0; i < subdivision_; ++i) {
      for (unsigned j = 0; j < 3; ++j) {
        triangle_strips_vertices_[6*i + j] = inner_vertices_[3*i +j];
        triangle_strips_vertices_[6*i + 3 + j] = outer_vertices_[3*i +j];
      }
    }

    all_dis_ = 0;
  }

  ~AnnulusNode() {
    if (all_dis_) glDeleteLists(all_dis_, 1);
  }

  void Init() {}
  void Sync() {}
  void Render() {
    Init();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    auto pose = to_opengl(mvp_matrix_);
    glMultMatrixf(&pose.front());

    if (all_dis_) {
      glCallList(all_dis_);
    } else {
      all_dis_ = glGenLists(1);
      glNewList(all_dis_, GL_COMPILE);

        glEnable( GL_LINE_SMOOTH );
        glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );

        glEnable(GL_BLEND); glCheckError();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); glCheckError();
        glColor(border_color_);
        glLineWidth(2.0f);
        glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
        glVertexPointer(3, GL_FLOAT, 0, &inner_vertices_.front()); glCheckError();
        glDrawArrays(GL_LINE_STRIP, 0, inner_vertices_.size()/3); glCheckError();
        glDisableClientState(GL_VERTEX_ARRAY); glCheckError();


        glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
        glVertexPointer(3, GL_FLOAT, 0, &outer_vertices_.front()); glCheckError();
        glDrawArrays(GL_LINE_STRIP, 0, outer_vertices_.size()/3); glCheckError();
        glDisableClientState(GL_VERTEX_ARRAY); glCheckError();

        glColor(face_color_);
        glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
        glVertexPointer(3, GL_FLOAT, 0, &triangle_strips_vertices_.front()); glCheckError();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, triangle_strips_vertices_.size()/3); glCheckError();
        glDisableClientState(GL_VERTEX_ARRAY); glCheckError();

        glDisable(GL_BLEND);

        glDisable( GL_LINE_SMOOTH );
      glEndList();

    }

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
  }

  float MiddleRadius() {
    return 1.0f - 0.5*(1.0f - inner_radius_);
  }

 protected:
  void paint();
  // Properties ----------------------------------------------------------------
  float inner_radius_;
  std::array<float, 4> border_color_;
  std::array<float, 4> face_color_;
  uint16_t subdivision_;   ///< number of subdivision (the higher the smoother)
  std::vector<float> inner_vertices_;
  std::vector<float> outer_vertices_;
  std::vector<float> triangle_strips_vertices_;

  GLuint all_dis_;

};
}  // namespace present