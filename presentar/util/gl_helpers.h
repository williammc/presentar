// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <array>
#include "presentar/core/datatypes.h"
#include "presentar/core/opengl.h"

namespace present {

inline void glCheckError(const char *file, int line) {
  GLenum err (glGetError());

  while(err!=GL_NO_ERROR) {
    std::string error;

    switch(err) {
    case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
    case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
    case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
    case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
//    case GL_INVALID_FRAMEBUFFER_OPERATION:
//      error="INVALID_FRAMEBUFFER_OPERATION";
//      break;
    }

    printf("GL_%s - %s:%d", error.c_str() , file, line);
    err = glGetError();
  }
}

#define glCheckError() glCheckError(__FILE__,__LINE__)

// check if a given Extension is supported by the equiped OpenGL
inline bool IsExtensionSupported(const char *extension) {
  const GLubyte *extensions = NULL;
  const GLubyte *start;
  GLubyte *where, *terminator;
  // Extension names should not have spaces.
  where = (GLubyte *)(strchr(extension, ' '));
  if (where || *extension == '\0')
    return false;

  extensions = glGetString(GL_EXTENSIONS);
  // It takes a bit of care to be fool-proof about parsing the
  // OpenGL extensions string. Don't be fooled by sub-strings, etc
  start = extensions;
  for (;;) {
    where = (GLubyte *)(strstr((const char *) start, extension));
    if (!where)
      break;

    terminator = where + strlen(extension);
    if (where == start || *(where - 1) == ' ')
      if (*terminator == ' ' || *terminator == '\0')
        return true;
    start = terminator;
  }
  return false;
}

#ifdef GL_GLEXT_PROTOTYPES
// check error message
inline int GLExtCheckFramebufferStatus(char *error_message) {
  GLenum status;
  status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  switch (status) {
  case GL_FRAMEBUFFER_COMPLETE:
    break;
  case GL_FRAMEBUFFER_UNSUPPORTED:
    // Choose different formats
    strcpy(error_message,
           "Framebuffer object format is unsupported by the video hardware. " \
           "(GL_FRAMEBUFFER_UNSUPPORTED_EXT)(FBO - 820)");
    return -1;
  case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
    strcpy(error_message, "Incomplete attachment." \
           "(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT)(FBO - 820)");
    return -1;
  case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
    strcpy(error_message, "Incomplete missing attachment." \
           " (GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT)(FBO - 820)");
    return -1;
  //case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
  //  strcpy(error_message, "Incomplete dimensions." \
  //         " (GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT)(FBO - 820)");
  //  return -1;
  default:
    // programming error; will fail on all hardware
    strcpy(error_message, "Some video driver error or" \
           " programming error occured. " \
           " Framebuffer object status is invalid. (FBO - 823)");
    return -2;
  }
  return 1;
}
#endif

inline Mat4f CalcProjectionMatrix(
  float right, float left, float top, float bottom, float z_near, float z_far) {
    Mat4f result;
    // First Column
    result(0, 0) = 2.0f * z_near / (right - left);
    result(1, 0) = 0.0f;
    result(2, 0) = 0.0f;
    result(3, 0) = 0.0f;

    // Second Column
    result(0, 1) = 0.0f;
    result(1, 1) = 2.0f * z_near / (top - bottom);
    result(2, 1) = 0.0f;
    result(3, 1) = 0.0f;

    // Third Column
    result(0, 2) = (right + left) / (right - left);
    result(1, 2) = (top + bottom) / (top - bottom);
    result(2, 2) = -(z_far + z_near) / (z_far - z_near);
    result(3, 2) = -1.0f;

    // Fourth Column
    result(0, 3) = 0.0f;
    result(1, 3) = 0.0f;
    result(2, 3) = -(2.0f * z_far * z_near) / (z_far - z_near);
    result(3, 3) = 0.0f;
    return result;
}

// sets a gl frustum from the linear camera parameters,
// image size and near and far plane.
// The camera will be in OpenGL style with camera center in the origin
// and the viewing direction
// down the negative z axis, with y pointing upwards
// and x pointing to the left and the image plane
// at z=-1.
// Images coordinates need to be rotated around the x axis
// to make sense here, because typically
// the camera is described as y going down (pixel lines) and image plane at z=1.
// @param params contains fu, fv, pu, pv as the linear part of camera parameters
// @param width width of the image plane in pixels
// @param height height of the image plane in pixels
// @param near near clipping plane
// @param far far clipping plane
inline Mat4f CalcProjectionMatrix(const Vec6f &params,
                      float width, float height,
                      float z_near = 0.1,
                      float z_far = 100) {
  float left, right, bottom, top;
  left = -z_near * params[2] / params[0];
  top = z_near * params[3] / params[1];
  right = z_near * (width - params[2]) / params[0];
  bottom = - z_near * (height - params[3]) / params[1];
//  ::glFrustum(left, right, bottom, top, z_near, farPlane);
  return CalcProjectionMatrix(right, left, top, bottom, z_near, z_far);
}

template <class CAMERA>
inline void CalcProjectionMatrix(const CAMERA & camera, float width, float height,
                      float z_near = 0.1, float z_far = 100) {
  CalcProjectionMatrix(camera.getParameters(), width, height, z_near, z_far);
}

inline Mat4f CalcOrthoProjection(const float left, const float right,
                               const float top, const float bottom,
                               float z_near, float z_far) {
  // frustum matrix: http://www.opengl.org/sdk/docs/man2/xhtml/glFrustum.xml
  Mat4f projection;
  projection.setIdentity();

  // First Column
  projection(0, 0) = 2.0f / (right - left);
  projection(1, 1) = 2.0f / (top - bottom);
  projection(2, 2) = - 2.0f / (z_far - z_near);

  // Fourth Column
  projection(0, 3) = -(right + left) / (right - left);
  projection(1, 3) = -(top + bottom) / (top - bottom);
  projection(2, 3) = -(z_far + z_near) / (z_far - z_near);

  return projection;
}


inline std::array<float, 16> to_opengl(const Mat4f& m) {
  std::array<float, 16> mgl;
  for (int r = 0; r < 4; r++)
      for (int c = 0; c < 4; c++)
          mgl[c+r*4] = m(c, r);  // tranpose => column-major order for opengl matrix
  return mgl;
}

template<typename SE3Type>
inline Mat4f to_mat4f(SE3Type const&pose) {
  Mat4f m4;
  m4.setIdentity();
  m4.template block<3, 3>(0, 0) = pose.get_rotation().get_matrix();
  m4.template block<3, 1>(0, 3) = pose.get_translation();
  return m4;
}

}  // namespace present