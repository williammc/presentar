// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "presentar/core/opengl.h"
#include "presentar/presentar_api.h"
namespace present {

struct PRESENTAR_API VideoMappingShader {
  VideoMappingShader() : vshader(0), fshader(0) {}
  ~VideoMappingShader() {
    if (vshader) glDeleteShader(vshader);
    if (fshader) glDeleteShader(fshader);
  }

  void Setup();
  void AttachParams(float* vertices, float* texcoords);
  void ReleaseParams();

  GLuint vshader, fshader, shader_prog;
  GLuint position_h, texcoord_h;
};

struct PRESENTAR_API RenderShader {
  RenderShader() : vshader(0), fshader(0) {}
  ~RenderShader() {
    if (vshader) glDeleteShader(vshader);
    if (fshader) glDeleteShader(fshader);
  }

  void Setup();
  void AttachParams(float* vertices, float* color, float* mvp_matrix);
  void ReleaseParams();


  GLuint vshader, fshader, shader_prog;
  GLuint position_h, color_h, mvp_h;
};

}  // namespace present