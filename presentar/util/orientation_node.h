// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "presentar/core/opengl.h"
#include "presentar/core/present_node.h"
#include "presentar/util/annulus_node.h"
#include "presentar/util/text_node.h"
#include "presentar/util/gl_draw_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
FORWARD_DECLARE_CLASS_SHARED_PTR(OrientationNode);

class PRESENTAR_API OrientationNode : public PresentNode {
 public:
  OrientationNode(const OrientationNode&) = delete;
  OrientationNode(const float scale = 1.0f,
                  uint32_t id = 0,
                  PresentNode *parent = nullptr)
  : scale_(scale), PresentNode(id, parent) {
    cube_dis_ = 0;
    text1_dis_ = 0;
    text2_dis_ = 0;
  }

  ~OrientationNode() {
    if (cube_dis_) glDeleteLists(cube_dis_, 1);
    if (text1_dis_) glDeleteLists(text1_dis_, 1);
    if (text2_dis_) glDeleteLists(text2_dis_, 1);
  }

  void Init() {
    if (initialized_) return;
    cube_vertices_ = {
      1.0f, 1.0f, -1.0f, // Top Right Of The Quad (Top)
      -1.0f, 1.0f, -1.0,  // Top Left Of The Quad (Top)
      -1.0f, 1.0f, 1.0f, // Bottom Left Of The Quad (Top)
      1.0f, 1.0f, 1.0f,// Bottom Right Of The Quad (Top)

      1.0f, -1.0f, 1.0f, // Top Right Of The Quad (Bottom)
      -1.0f, -1.0f, 1.0,  // Top Left Of The Quad (Bottom)
      -1.0f, -1.0f, -1.,  // Bottom Left Of The Quad (Bottom)
      1.0f, -1.0f, -1.0,  // Bottom Right Of The Quad (Bottom)

      1.0f, 1.0f, 1.0f,// Top Right Of The Quad (Front)
      1.0f, -1.0f, 1.0f, // Bottom Right Of The Quad (Front)
      -1.0f, -1.0f, 1.0,  // Bottom Left Of The Quad (Front)
      -1.0f, 1.0f, 1.0f, // Top Left Of The Quad (Front)

      1.0f, -1.0f, -1.0,  // Bottom Left Of The Quad (Back)
      1.0f, 1.0f, -1.0f, // Top Left Of The Quad (Back)
      -1.0f, 1.0f, -1.0,  // Top Right Of The Quad (Back)
      -1.0f, -1.0f, -1.,  // Bottom Right Of The Quad (Back)

      -1.0f, 1.0f, 1.0f, // Top Right Of The Quad (Left)
      -1.0f, 1.0f, -1.0,  // Top Left Of The Quad (Left)
      -1.0f, -1.0f, -1.,  // Bottom Left Of The Quad (Left)
      -1.0f, -1.0f, 1.0,  // Bottom Right Of The Quad (Left)

      1.0f, 1.0f, -1.0f, // Top Right Of The Quad (Right)
      1.0f, 1.0f, 1.0f,// Top Left Of The Quad (Right)
      1.0f, -1.0f, 1.0f, // Bottom Left Of The Quad (Right)
      1.0f, -1.0f, -1.0,  // Bottom Right Of The Quad (Right)
    };

    annulus_node_.reset(new AnnulusNode(0.7f,
                                    std::array<float, 4>{1.0f, 1.0f, 1.0f, 1.0f},
                                    std::array<float, 4>{0.7f, 0.7f, 0.7f, 0.7f},
                                    300, 0, this));
    initialized_ = true;
  }

  void Sync() {}

  void Update() {
    Render();
  }

  void Render() {
    Init();

    Mat4f mscale = Mat4f::Identity()*scale_;
    Mat4f mvp = vp_matrix_ * mscale * cube_modelmatrix_;
    const float intensity = 0.7;

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    auto pose = to_opengl(mvp);
    glMultMatrixf(&pose.front());

    if (cube_dis_) {
      glCallList(cube_dis_);
    } else {
      cube_dis_ = glGenLists(1);
      glNewList(cube_dis_, GL_COMPILE);

        glColor4f(intensity, intensity, intensity, 1.0f);
        glLineWidth(2.0f);
        glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
        glVertexPointer(3, GL_FLOAT, 0, &cube_vertices_.front()); glCheckError();
        glDrawArrays(GL_QUADS, 0, cube_vertices_.size()/3); glCheckError();
        glDisableClientState(GL_VERTEX_ARRAY); glCheckError();

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
        glVertexPointer(3, GL_FLOAT, 0, &cube_vertices_.front()); glCheckError();
        glDrawArrays(GL_LINES, 0, cube_vertices_.size()/3); glCheckError();
        glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
      glEndList();

    }

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    const float ann_scale = scale_*2.5f;
    mscale.setIdentity();
    mscale *= ann_scale;
    Mat4f rot_90x;
    const float angle = -M_PI / 2;
    rot_90x << 1, 0, 0, 0,
               0, cos(angle), -sin(angle), 0,
               0, sin(angle), cos(angle), 0,
               0, 0, 0, 1;

    Mat4f mvp_annulus;
    mvp_annulus = vp_matrix_ * mscale * rot_90x;
#if 0
    annulus_node_->SetModelViewProjection(mvp_annulus);
    annulus_node_->Update();
#endif

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    /// center to origin, aligned to +X, -Y directions, facing toward +Z
    /// in Vision coordinates
    auto glDrawTextXY = [&](std::string text, const float text_scale) {
      glPushMatrix();
      glRotatef(180, 1, 0, 0);  // from  OpenGL coords to vision coords
      glScalef(text_scale, text_scale, text_scale);
      glTranslatef(-1.0f/text.length(), -1.0f/text.length(), 0.0f);
      glDrawText(text);
      glPopMatrix();
    };

    if (0) {  // draw base text around annulus 3D
      const float text_scale = 0.4f;
      const float text_alpha = 0.5;
      const float ann_mid_bder = annulus_node_->MiddleRadius();

      std::array<float, 16> pose;
      int n = 0;
      for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j, n++)
          pose[n] = mvp_annulus(j, i);  // changed to column-major order
      glPushMatrix();
      glMultMatrixf(&pose.front());

      if (text1_dis_) {
        glCallList(text1_dis_);
      } else {
        text1_dis_ = glGenLists(1);
        glNewList(text1_dis_, GL_COMPILE);

          glColor4f(0.0f, 0.0f, 1.0f, text_alpha);
          glPushMatrix();
          glTranslatef(0.0f, -ann_mid_bder, 0.0f);
          glDrawTextXY(std::string("+Z_w"), text_scale);
          glPopMatrix();


          glPushMatrix();
          glTranslatef(0.0f, ann_mid_bder, 0.0f);
          glDrawTextXY(std::string("-Z_w"), text_scale);
          glPopMatrix();

          glColor4f(1.0f, 0.0f, 0.0f, text_alpha);
          glPushMatrix();
          glTranslatef(-ann_mid_bder, 0.0f, 0.0f);
          glRotatef(90, 0, 0, 1);
          glDrawTextXY(std::string("-X_w"), text_scale);
          glPopMatrix();


          glPushMatrix();
          glTranslatef(ann_mid_bder, 0.0f, 0.0f);
          glRotatef(90, 0, 0, 1);
          glDrawTextXY(std::string("+X_w"), text_scale);
          glPopMatrix();

        glEndList();
      }

      glPopMatrix();
    }

    if (0) { // draw text on the cube
      const float text_scale = 1.0f;
      std::array<float, 16> pose;
      int n = 0;
      for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j, n++)
          pose[n] = mvp(j, i);  // changed to column-major order
      glPushMatrix();
      glMultMatrixf(&pose.front());


      if (text2_dis_) {
        glCallList(text2_dis_);
      } else {
        text2_dis_ = glGenLists(1);
        glNewList(text2_dis_, GL_COMPILE);

          glScalef(1.1f, 1.1f, 1.1f);  // make the text going out of the box

          // x-axis
          glColor4f(0.7f, 0.0f, 0.0f, 0.8f);
          glPushMatrix();
          glTranslatef(1.0f, 0.0f, 0.0f);
          glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
          glDrawTextXY(std::string("+X"), text_scale);
          glPopMatrix();

          glPushMatrix();
          glTranslatef(-1.0f, 0.0f, 0.0f);
          glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
          glDrawTextXY(std::string("-X"), text_scale);
          glPopMatrix();

          // y-axis
          glColor4f(0.0f, 0.7f, 0.0f, 0.8f);
          glPushMatrix();
          glTranslatef(0.0f, 1.0f, 0.0f);
          glRotatef(90, 1.0f, 0.0f, 0.0f);
          glDrawTextXY(std::string("+Y"), text_scale);
          glPopMatrix();

          glPushMatrix();
          glTranslatef(0.0f, -1.0f, 0.0f);
          glRotatef(-90, 1.0f, 0.0f, 0.0f);
          glDrawTextXY(std::string("-Y"), text_scale);
          glPopMatrix();

          // z-axis
          glColor4f(0.0f, 0.0f, 0.7f, 0.8f);

          glPushMatrix();
          glTranslatef(0.0f, 0.0f, 1.0f);
          glRotatef(180, 0.0f, 1.0f, 0.0f);
          glDrawTextXY(std::string("+Z"), text_scale);
          glPopMatrix();

          glPushMatrix();
          glTranslatef(0.0f, 0.0f, -1.0f);
          glDrawTextXY(std::string("-Z"), text_scale);
          glPopMatrix();

        glEndList();
      }
      glPopMatrix();
    }

    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
  }

  void SetCubeModel(const Mat4f& m) {
    cube_modelmatrix_ = m;
  }

  void SetViewProjection(const Mat4f& vp) {
    vp_matrix_ = vp;
  }

 protected:
  void paint();
  // Properties ----------------------------------------------------------------
  float scale_;

  std::array<float, 72> cube_vertices_;
  AnnulusNodePtr annulus_node_;
  Mat4f cube_modelmatrix_;  ///< in Vision coordinates
  Mat4f vp_matrix_;         ///< frusturm_matrix * rot_180x * view_matrix;

  GLuint cube_dis_, text1_dis_, text2_dis_;
};
}  // namespace present