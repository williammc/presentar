#pragma once 
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include "presentar/core/defer.h"

namespace present {
  // A picture.   
  typedef struct {
    std::vector<uint8_t> pixels;
    size_t width;
    size_t height;
    uint8_t pixel_size;
    uint8_t bit_depth;
  } bitmap_t;

  /// Given "bitmap", this returns the pixel of bitmap at the point ("x", "y").
  static uint8_t * pixel_at(bitmap_t& bitmap, int x, int y) {
    const uint8_t bytecount = bitmap.pixel_size * bitmap.bit_depth / 8;
    return &bitmap.pixels.front() + (bitmap.width * y + x) * bytecount;
  }

  /// Write "bitmap" to a PNG file specified by "path"; returns 0 on 
  /// success, non-zero on error.
  inline int write_png_file(bitmap_t& bitmap, const char *path) {
    // "status" contains the return value of this function. At first
    // it is set to a value which means 'failure'. When the routine
    // has finished its work, it is set to a value which means
    // 'success'.
    // The following number is set by trial and error only. I cannot
    //   see where it it is documented in the libpng manual.
    auto pixel_type = PNG_COLOR_TYPE_RGB;
    if (bitmap.pixel_size == 1) {
      if (bitmap.bit_depth == 8 || bitmap.bit_depth == 16)
        pixel_type = PNG_COLOR_TYPE_GRAY;
      else {
        return - 1;
      }
    } else if (bitmap.pixel_size == 3 && bitmap.bit_depth == 8) {
      pixel_type = PNG_COLOR_TYPE_RGB;
    } else if (bitmap.pixel_size == 4 && bitmap.bit_depth == 8) {
      pixel_type = PNG_COLOR_TYPE_RGB_ALPHA;
    } else {
      return -1;
    }

  FILE* fp = fopen(path, "wb");
  if (!fp) return -1;

  auto png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
  if (png_ptr == nullptr)
    return -1;

  auto info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == nullptr)
    return -1;

  auto close_defer = present::defer([&]() { 
    /*if (png_ptr != nullptr && info_ptr != nullptr)
      png_destroy_write_struct(&png_ptr, &info_ptr);*/

    if (fp)
      fclose(fp);
  });


  // Set up error handling.
  if (setjmp(png_jmpbuf(png_ptr)))
    return -1;

  // Set image attributes.
  png_set_IHDR(png_ptr,
               info_ptr,
               bitmap.width,
               bitmap.height,
               bitmap.bit_depth,
               pixel_type,
               PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT,
               PNG_FILTER_TYPE_DEFAULT);

  // Initialize rows of PNG.
  png_byte ** row_pointers = (png_byte**) png_malloc(png_ptr, bitmap.height * sizeof(png_byte *));
  for (size_t y = 0; y < bitmap.height; ++y) {
    png_byte *row = (png_byte*)
      png_malloc(png_ptr, sizeof(uint8_t) * bitmap.width * bitmap.pixel_size);
    row_pointers[y] = row;
    for (size_t x = 0; x < bitmap.width; ++x) {
      uint8_t * pixel = pixel_at(bitmap, x, y);
      const uint8_t bytecount = bitmap.pixel_size * bitmap.bit_depth / 8;
      for (int i = 0; i < bytecount; ++i)
        *row++ = *pixel++;
    }
  }

  // Write the image data to "fp".
  png_init_io(png_ptr, fp);
  png_set_rows(png_ptr, info_ptr, row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);

  // The routine has successfully written the file
  for (size_t y = 0; y < bitmap.height; y++) {
    png_free(png_ptr, row_pointers[y]);
  }
  png_free(png_ptr, row_pointers);
  return 0;
}

/// read a png image from file
inline int read_png_file(const char* file_name, bitmap_t& bitmap) {
  char header[8];    // 8 is the maximum size that can be checked

  // open file and test for it being a png
  FILE *fp = fopen(file_name, "rb");
  if (!fp) return -1;

  fread(header, 1, 8, fp);
  if (png_sig_cmp((png_const_bytep)header, 0, 8))
    return -1;

  // initialize stuff
  auto png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

  if (!png_ptr)
    return -1;

  auto info_ptr = png_create_info_struct(png_ptr);

  auto close_df = present::defer([&]() {
    /*if (png_ptr != nullptr && info_ptr != nullptr);
      png_destroy_read_struct(&png_ptr, &info_ptr, &info_ptr);*/
    
    if (fp)
      fclose(fp);
  });

  if (!info_ptr)
    return -1;

  if (setjmp(png_jmpbuf(png_ptr))) 
    return -1;

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  auto width = png_get_image_width(png_ptr, info_ptr);
  auto height = png_get_image_height(png_ptr, info_ptr);
  auto color_type = png_get_color_type(png_ptr, info_ptr);
  auto bit_depth = png_get_bit_depth(png_ptr, info_ptr);

  auto number_of_passes = png_set_interlace_handling(png_ptr);
  png_read_update_info(png_ptr, info_ptr);

  // read file 
  if (setjmp(png_jmpbuf(png_ptr)))
    return -1;

  auto row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
  for (int y = 0; y < height; y++)
    row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr, info_ptr));
   
  png_read_image(png_ptr, row_pointers);

  bitmap.width = width;
  bitmap.height = height;
  bitmap.bit_depth = bit_depth;

  switch (color_type) {
  case PNG_COLOR_TYPE_GRAY:
    bitmap.pixel_size = 1;
    break;
  case PNG_COLOR_TYPE_RGB:
    bitmap.pixel_size = 3;
    break;
  case PNG_COLOR_TYPE_RGB_ALPHA:
    bitmap.pixel_size = 4;
    break;
  default:
    return -1;
  }

  const uint8_t bytecount = bitmap.pixel_size * bitmap.bit_depth / 8;
  bitmap.pixels.resize(width * height * bytecount);
  for (int y = 0; y < height; y++) {
    uint8_t* row = row_pointers[y];
    for (int x = 0; x < width; ++x) {
      auto pixel = pixel_at(bitmap, x, y);
      
      for (int i = 0; i < bytecount; ++i)
        *pixel++ = *row++;
    }
  }

  return 0;
}

}  // namespace present