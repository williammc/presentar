// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <array>
#include "presentar/core/present_node.h"
#include "presentar/util/gl_draw_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
class PRESENTAR_API TriangleNode : public PresentNode {
 public:
  TriangleNode(const TriangleNode&) = delete;
  TriangleNode(uint32_t id = 0, PresentNode *parent = nullptr)
      : PresentNode(id, parent) {
    pos = std::array<float, 9> {-0.02f, -0.01f, 1.0f,
                                0.02f, -0.01f, 1.0f,
                                0.0f,  0.1f, 1.0f};
    const float alpha = 1.0f;
    colors = std::array<float, 12> {1.0f, 0.0f, 0.0f, alpha,
                                    0.0f, 1.0f, 0.0f, alpha,
                                    0.0f, 0.0f, 1.0f, alpha};
  }

  TriangleNode(const std::array<float, 9> tri_pos,
               const std::array<float, 12> tri_colors,
               uint32_t id = 0,
               PresentNode *parent = nullptr)
      : PresentNode(id, parent), pos(tri_pos), colors(tri_colors){
  }

  ~TriangleNode() {}

  void Init() {
  }

  void Render() {
    glDrawTriangle(pos, colors);
  }

  void Sync() {
    ComputeBBox();
  }

  void ComputeBBox() {
    for (size_t i = 0; i < pos.size(); i += 3) {
      bbox_.min_x = std::min(bbox_.min_x, pos[i]);
      bbox_.min_y = std::min(bbox_.min_y, pos[i+1]);
      bbox_.min_z = std::min(bbox_.min_z, pos[i+2]);

      bbox_.max_x = std::max(bbox_.max_x, pos[i]);
      bbox_.max_y = std::max(bbox_.max_y, pos[i+1]);
      bbox_.max_z = std::max(bbox_.max_z, pos[i+2]);
    }
  }

 protected:
  // Properties ----------------------------------------------------------------
  std::array<float, 9> pos;
  std::array<float, 12> colors;
};

typedef std::shared_ptr<TriangleNode> TriangleNodePtr;
}  // namespace present