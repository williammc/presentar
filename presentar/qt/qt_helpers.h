#pragma once
#include "presentar/core/datatypes.h"

namespace present {

inline QMatrix4x4 to_qt(const Mat4f& m) {
  QMatrix4x4 mqt;
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j) mqt(i, j) = m(i, j);
  return mqt;
}

inline Mat4f to_eigen(const QMatrix4x4& mqt) {
  Mat4f m;
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j) m(i, j) = mqt(i, j);
  return m;
}
}  // namespace present