#pragma once
#include <sstream>
#include "presentar/core/opengl.h"  // this must go first
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QtOpenGL/QGLFramebufferObject>

#include "presentar/qt/present_quick_item.h"
#include "presentar/qt/qt_helpers.h"
#include "presentar/presentar_api.h"

namespace present {

// For RGB-D scene rendering
class PRESENTAR_API SimpleScene final : public AbstractQuickItem {
  Q_OBJECT
 public:
  SimpleScene();
  ~SimpleScene();

  // implementing AbstractQuickItem
  void initialize() {
    if (initialized_) return;
    initialized_ = true;

    QMatrix4x4 proj_matrix = to_qt(CalcProjectionMatrix(1, -1, 1, -1, 0.0f, 1.0f));
    QMatrix4x4 rot_180x;
    rot_180x.setToIdentity();
    rot_180x.rotate(180, QVector3D(1.0, 0.0, 0.0));
    auto m = proj_matrix*rot_180x;
    mvp_matrix_ = to_eigen(m);

    addRendableObject(TriangleNodePtr(new TriangleNode()));
  }


 public slots:
  void paint() {
    root_->Update();
  }

  void cleanup() {}
  void sync() {
    root_->SetModelViewProjection(mvp_matrix_);
  }

 protected:
  Mat4f mvp_matrix_;
};
}  // namespace present