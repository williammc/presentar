#pragma once
#include <sstream>
#include <time.h>

#include "presentar/core/opengl.h"  // this must be included first
#include <QQuickFramebufferObject>
#include <QQuickItem>
#include <QQuickWindow>
#include <QOpenGLContext>
#include <QOpenGLFunctions_4_1_Compatibility>
#include <QOpenGLDebugLogger>
#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTimeMonitor>
#include <QOpenGLFramebufferObjectFormat>
#include <QSGGeometry>
#include <QSGGeometryNode>
#include <QSGSimpleRectNode>
#include <QSGSimpleTextureNode>

#include "presentar/core/present_node.h"
#include "presentar/presentar_api.h"

namespace present {
class AbstractRenderer : public QQuickFramebufferObject::Renderer {
 public:
  AbstractRenderer(PresentQuickItem* item) : item_(item) {
  }

  void render() {
    item_->update();
  }

  void synchronize(QQuickFramebufferObject * item) {
    Renderer::synchronize(item);
    if (dynamic_cast<PresentQuickItem*>(item))
      dynamic_cast<PresentQuickItem*>(item)->sync();
  }

  /// must have this to enable depth test
  QOpenGLFramebufferObject *createFramebufferObject(const QSize &size) {
    QOpenGLFramebufferObjectFormat format;
    if (item_->needDepth() && item_->needStencil()){
      format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
      format.setSamples(2);
    } else if (item_->needDepth()) {
      format.setAttachment(QOpenGLFramebufferObject::Depth);
      format.setSamples(2);
    } else {
      format.setAttachment(QOpenGLFramebufferObject::NoAttachment);
    }
    return new QOpenGLFramebufferObject(size, format);
  }

  QOpenGLFramebufferObject* framebufferObject() {
    return Renderer::framebufferObject();
  }

 private:
  PresentQuickItem* item_;

};


/// Represents a renderable scene item (aka a Qt Scene Graph element)
class PRESENTAR_API PresentQuickItem : public QQuickFramebufferObject {
  Q_OBJECT
 typedef QOpenGLFunctions_4_1_Compatibility  QOpenGLFunctionsType;
 Q_PROPERTY(bool isBackground READ isBackground WRITE setIsBackground NOTIFY isBackgroundChanged)
 public:
  PresentQuickItem(const PresentQuickItem&) = delete;
  PresentQuickItem(QQuickItem* parent = nullptr) 
    : QQuickFramebufferObject(parent), is_bg_(false) {
    ess_initialized_ = false;
    initialized_ = false;
    gl_time_monitor_ = nullptr;
    gl_debug_logger_ = nullptr;
  }

  virtual ~PresentQuickItem() {
    delete gl_time_monitor_;
    delete gl_debug_logger_;
    cleanup();
  }

  /// fix inverted-y in QOpenGLFramebufferObject
  /// @ref: http://qt-project.org/forums/viewthread/25408
  /// @ref: https://qt.gitorious.org/qt/qt5declarative/commit/f5ccfb153fccb620a478e2cac3a29726c8205a50
  QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData * data) {
    auto n = QQuickFramebufferObject::updatePaintNode(node, data);
    auto n1 = dynamic_cast<QSGSimpleTextureNode*>(n);
    if (n1) {
      QSGGeometry::updateTexturedRectGeometry(n1->geometry(),
                                              QRectF(0, 0, width(), height()),
                                              QRectF(0, 1, 1, -1));
    }
    return n;
  }

  virtual bool needDepth() { return false;}
  virtual bool needStencil() { return false; }
  
  /// this needs needDepth() & needStencil() to create approriate FBO
  Renderer* createRenderer() const {
    const_cast<PresentQuickItem*>(this)->renderer_ = new AbstractRenderer(const_cast<PresentQuickItem*>(this));
    return renderer_;
  }

  QOpenGLFramebufferObject* framebufferObject() const {
    return dynamic_cast<AbstractRenderer*>(renderer_)->framebufferObject();
  }

  QOpenGLContext* context() const { return window()->openglContext(); }

  virtual void initialize() = 0;    ///< doing necessary initialization
  void addRendableObject(PresentNodePtr);

  Q_INVOKABLE bool isOffScene() const {
    return !(x() > 0 || y() > 0 || x() + width() > 0 || y()+height() > 0);
  }

  Q_INVOKABLE void captureFrame() const {  ///< capture current frame and store to image
    static int cap_id = 0;
    std::string fn = "captured_frame_" + std::to_string(cap_id++) + ".png";
    printf("Write captured image to %s\n", fn.c_str());

    QImage image = window()->grabWindow();
    image.save(QString(fn.c_str()));
  }
  
  bool isBackground() const { return is_bg_; }
  void setIsBackground(const bool on) { is_bg_ = on; }

signals:
  void viewoptionChanged() const;
  void isBackgroundChanged() const;

public slots:
  Q_INVOKABLE virtual void update();
  void essentialInitialize() {  ///< common necessary initialization{
    if (ess_initialized_) return;
    ess_initialized_ = true;

  #ifdef WIN32
    static bool first = true;
    if (first) {  // only initialize glew once
      if (GLEW_OK != glewInit()) {
        printf("GLEW init failed\n");
        exit(-1);
      }
      first = false;
    }
  #endif

    qDebug("OpenGL initialized: version: %s GLSL: %s",
           glGetString(GL_VERSION),\
           glGetString(GL_SHADING_LANGUAGE_VERSION));
    if (window()) {
      if (window()->openglContext()) {
        connect(window()->openglContext(), SIGNAL(aboutToBeDestroyed()),
                this, SLOT(cleanup()), Qt::DirectConnection);
        //    window()->openglContext()->setFormat(window()->format());

        // gl_funcs_ = context()->versionFunctions<QOpenGLFunctionsType>();
        // if (gl_funcs_) gl_funcs_->initializeOpenGLFunctions();
      }
    }

  #if !defined(__APPLE__) && defined(_DEBUG) && 0
    // Create a timer query object
    gl_time_monitor_ = new QOpenGLTimeMonitor(this);
    gl_time_monitor_->setSampleCount(4);
    if (!gl_time_monitor_->create())
      qWarning() << "failed to create timer query object";

    gl_debug_logger_ = new QOpenGLDebugLogger(this);

    connect(gl_debug_logger_, SIGNAL(messageLogged(QOpenGLDebugMessage)), this,
            SLOT(onMessageLogged(QOpenGLDebugMessage)), Qt::DirectConnection);

    if (gl_debug_logger_->initialize()) {
      gl_debug_logger_->startLogging(QOpenGLDebugLogger::SynchronousLogging);
      gl_debug_logger_->enableMessages();
    }
  #endif
    setTextureFollowsItemSize(true);

    root_.reset(new AbstractObject3D());
  }

  virtual void paint() = 0;     ///< paint graphics content
  virtual void cleanup() {}     ///< done before the QuickView closing down
  virtual void sync() {         ///< used to synchronize values (QML & this)
    root_->Sync();
  }
    
  void onMessageLogged(QOpenGLDebugMessage message) { qDebug() << message; }

protected:
  virtual void doBeforePaint() {
    render_stages_.clear();
    // store current GL states of Qt QMl (return to this state afterward)
    glPushAttrib(GL_ALL_ATTRIB_BITS); glCheckError();
    glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS); glCheckError();
    initialize();
  }

  virtual void doAfterPaint() {
    // make sure GL state as before for QML to work
    glPopClientAttrib(); glCheckError();
    glPopAttrib(); glCheckError();
    readRecordedTimeSamples(); glCheckError();
  }

  // for debug info gathering
  void readRecordedTimeSamples() {
    if (gl_time_monitor_ == nullptr) return;
    // Block until the results are available
    QVector<GLuint64> time_samples = gl_time_monitor_->waitForSamples();
    qDebug() << "timeSamples =" << time_samples;

    QVector<GLuint64> intervals = gl_time_monitor_->waitForIntervals();
    debug_ss_ << "intervals.count():" << intervals.count()
              << "render_stages_[i]:%d\n " << render_stages_.size();
    for (int i = 0;
         i < std::min(int(render_stages_.size()), int(intervals.count())); ++i)
      debug_ss_ << i << render_stages_[i].c_str()
                << double(intervals.at(i)) / 1.0e6 << "msecs";

    qDebug() << debug_ss_.str().c_str();
    // Clear the query results ready for the next frame
    gl_time_monitor_->reset();
  }

  std::string getDebugInfoAndClear() {
    std::string debug_info = debug_ss_.str();
    debug_ss_.str("");
    return debug_info;
  }

  // Properties ----------------------------------------------------------------
protected:
  Renderer* renderer_;
  bool ess_initialized_;
  bool initialized_;
  QOpenGLFunctionsType* gl_funcs_;  ///< for calling modern OpenGL 2.0+ functions
  bool is_bg_;

  // for debugging stuffs
  QOpenGLTimeMonitor* gl_time_monitor_;
  QOpenGLDebugLogger* gl_debug_logger_;
  std::vector<std::string> render_stages_;
  std::stringstream debug_ss_;

  PresentNodePtr root_;  ///< containing rendable objects to be renderred
};
}  // namespace present