#pragma once
#include "presentar/qt/present_quick_item.h"
#include <sstream>

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QtOpenGL/QGLFramebufferObject>

#include "presentar/core/texture_node.h"
#include "presentar/presentar_api.h"

namespace present {

struct VideoShader;

// For RGB-D scene rendering
class PRESENTAR_API TextureScene : public PresentQuickItem {
  Q_OBJECT
  Q_PROPERTY(QImage theTexture WRITE setTheTexture)
 public:
  TextureScene(QQuickItem* parent = nullptr) : PresentQuickItem(parent) {}
  ~TextureScene() {}

  Q_INVOKABLE void setTheTexture(const QImage& image) {
    QMutexLocker locker(&tex_sync_);

    int nbytes = image.byteCount();
    if (nbytes > 0) {
      tex_size_ = image.size();
      nbytes_perbit_ = image.depth()/8;
      tex_pixels_.resize(nbytes);
      memcpy(&tex_pixels_.front(), image.bits(), nbytes);
      tex_format_ = (nbytes_perbit_ == 3) ? GL_RGB : GL_RGBA;
    }
  }

  // implementing AbstractQuickItem
  void initialize() {
    if (tex_pixels_.empty()) return;
    if (initialized_) return;
    if (nbytes_perbit_ == 3) {
      tex_node_.reset(new TextureNode(tex_size_));
    } else {
      tex_node_.reset(new TextureNode(tex_size_));
    }
    tex_node_->Init();
    initialized_ = true;
  }

 public slots:
  void paint() {
    glClear(GL_DEPTH_BUFFER_BIT); glCheckError();
    QMutexLocker locker(&tex_sync_);
    if (!tex_pixels_.empty() && video_shader_) {
      tex_node_->SetTexture(tex_pixels_, tex_format_);
      video_shader_->update(tex_pixels_);
      video_shader_->draw();
    }
  }

  void cleanup() {}
  void sync() {}

 protected:
  // Properties ----------------------------------------------------------------
  QSize tex_size_;
  unsigned nbytes_perbit_;
  std::vector<unsigned char> tex_pixels_;
  int tex_format_;
  QMutex tex_sync_;
  TextureNodePtr tex_node_;
};
}  // namespace present