#pragma once
#include <QObject>

namespace presentar {
struct QRenderOption;
typedef QSharedPointer<QRenderOption> QRenderOptionPtr;

/// Abstract ViewOption for any view item (derived from AbstractQuickItem)
struct PRESENTAR_API QRenderOption : QObject, RenderOption {
  Q_OBJECT
 public:
  QRenderOption() {}
  QRenderOption(const QRenderOption&) {}

  virtual ~QRenderOption() {}

  virtual unsigned option() const = 0;
  virtual void setOption(const unsigned opt) = 0;
};

}  // namespace presentar