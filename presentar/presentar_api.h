// Description: import/export macros for creating DLLS with MSVC
// Use SM_<module>_API macro to declare it.
// Sparate macros for each DLL required.
#pragma once

#ifdef _MSC_VER  // Microsoft compiler:
  #ifdef PresentAR_SHARED_LIBS
    #ifdef presentar_EXPORTS
      #define PRESENTAR_API __declspec(dllexport)
    #else
      #define PRESENTAR_API __declspec(dllimport)
    #endif
  #else
    #define PRESENTAR_API
  #endif
#else  // not MSVC
  #define PRESENTAR_API
#endif