// Copyright 2014 The PresentAR Authors. All rights reserved.
#include "presentar/core/ar_node.h"
#include <fstream>
#include "slick/scene/poli_camera.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/util/png_io.h"

namespace present {

template<typename CameraModel>
ARNode<CameraModel>::ARNode(const VecXf& cam_params,
                            float z_near, float z_far,
                            uint32_t id, PresentNode* p) 
  : PresentViewer(id, p),
    u_system_framebuffer(0), texture_mapping_nsteps_(24),
    camera_(cam_params),
    video_size_(cam_params.head<2>()),
    step1_display_(0), step2_display_(0) {
  render_mode_ = RenderMode::ORIGINAL;
  toset_render_mode_ = render_mode_;

  SetNearFarClip(z_near, z_far);
  Configure();
}

template<typename CameraModel>
ARNode<CameraModel>::~ARNode() {
  glDeleteTextures(1, &video_tex_);
  glDeleteBuffers(1, &buffer_);
  glDeleteTextures(1, &buffer_texture_);

  if (step1_display_) glDeleteLists(step1_display_, 1);
  if (step2_display_) glDeleteLists(step2_display_, 1);
}

template<typename CameraModel>
void ARNode<CameraModel>::Init() {
  SetProjectionMatrix(CalcProjectionMatrix(z_near_, z_far_));
}

template<typename CameraModel>
void ARNode<CameraModel>::Configure() {
  videoframe_pixels_.resize(video_size_[0]*video_size_[1]*3);
  // work out FrameBuffer size to cover undistorted video frame
  const Vec2f tl = camera_.Project(camera_.LinearUnProject(
                                    Vec2f(0.0, 0.0)));
  const Vec2f br = camera_.Project(camera_.LinearUnProject(
         Vec2f(video_size_[0], video_size_[1])));
  Vec2f offset = br - Vec2f(video_size_[0], video_size_[1]);
  if (offset.norm() < tl.norm()) {
    offset = -tl;
  }
  float max_ratio = std::max( 
    (video_size_[0] + offset[0]) / video_size_[0],
    (video_size_[1] + offset[1])/video_size_[1]);
  max_ratio *= 1.2;  // larger to surely cover undistorted video frame texture

  buffer_size_[0] = (int)video_size_[0]*max_ratio;
  buffer_size_[1] = (int)video_size_[1]*max_ratio;
    
  CalcUndistortionInfo(camera_, undist_info_, texture_mapping_nsteps_);
#if 1  // export debug info
  std::ofstream ofs("undist.txt");
  auto output_points = [&](const std::vector<float>& points) {
    for (int i = 0; i < points.size(); i += 2) {
      ofs << points[i] << ", " << points[i + 1] << ";\n";
    }
  };

  ofs << "d2u_texcoords=[\n";
  output_points(undist_info_.d2u_map.texcoords);
  ofs << "];\n";

  ofs << "d2u_ver=[\n";
  output_points(undist_info_.d2u_map.vertices);
  ofs << "];\n";
  ofs.close();

#endif

  texture_initialized_ = false;
}

template<typename CameraModel>
void ARNode<CameraModel>::Render() {
  if (!texture_initialized_) {
    GenerateFrameBuffers();
  }

  if (toset_render_mode_ != render_mode_)
    render_mode_ = toset_render_mode_;

  UploadVideoFrame(); glCheckError();

#if 1  // uncomment to get undistorted image
  SaveSnapshot("undistorted_image");
#endif

  if (render_mode_ == RenderMode::ORIGINAL)
    return;

  glEnable(GL_DEPTH_TEST); glCheckError();

  for (auto& child : children_) {
    child->Update();
  }

  glDisable(GL_DEPTH_TEST); glCheckError();

  // map FBO Texture to default FBO
  MapTextureToFBO(Rect{ 0, 0, width(), height() }, 0); glCheckError();

  glBindFramebuffer(GL_FRAMEBUFFER, u_system_framebuffer); glCheckError();
  glViewport(0, 0, width(), height()); glCheckError();
}

template<typename CameraModel>
void ARNode<CameraModel>::SaveSnapshot(std::string filename) {
  auto buf = buffer_;
  auto buf_size = buffer_size_;
  if (render_mode_ == RenderMode::ORIGINAL) {
    buf = 0;
    buf_size = window_size_;
  }
  std::vector<unsigned char> pixels(buf_size[0] * buf_size[1] * 3);
  glBindFramebuffer(GL_FRAMEBUFFER, buf);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glReadPixels(0, 0, buf_size[0], buf_size[1],
               GL_RGB, GL_UNSIGNED_BYTE, &pixels.front());

  bitmap_t bitmap{ pixels, buf_size[0], buf_size[1], 3, 8 };
  filename += ".png";
  write_png_file(bitmap, filename.c_str());
}

template<typename CameraModel>
Mat4f ARNode<CameraModel>::CalcProjectionMatrix(const float z_near, const float z_far) {
  Mat4f mfrus = present::CalcProjectionMatrix(undist_info_.frustum.right,
                                     undist_info_.frustum.left,
                                     undist_info_.frustum.top,
                                     undist_info_.frustum.bottom,
                                     z_near, z_far);
  return mfrus;
}

template<typename CameraModel>
void ARNode<CameraModel>::UploadVideoFrame() {
  if (videoframe_pixels_.empty()) return;

  std::vector<unsigned char>& pixels = videoframe_pixels_;

  // uploads the image frame to the frame texture
  glBindTexture(GL_TEXTURE_2D, video_tex_); glCheckError();
  glTexSubImage2D(GL_TEXTURE_2D,
                  0, 0, 0,
                  video_size_[0], video_size_[1],
                  vtexture_format_,
                  GL_UNSIGNED_BYTE,
                  &pixels.front()); glCheckError();

  // map frame texture to FBO / window-default-provided FBO
  if (render_mode_ == RenderMode::ORIGINAL) {
    // binding to window-system-provided FrameBuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0); glCheckError();
    glViewport(0, 0, width(), height());
  } else {  // UNDISTORT / DISTORT
    glBindFramebuffer(GL_FRAMEBUFFER, buffer_); glCheckError();
    glViewport(0, 0, buffer_size_[0], buffer_size_[1]);
  }
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glDisable(GL_BLEND);
  glEnable(GL_TEXTURE_2D); glCheckError();
  if (render_mode_ == RenderMode::ORIGINAL) {
    // draw orders = 0, 1, 2, 0, 2, 3
    // bl->br->tr->bl->tr->tl
    GLfloat fa_texcoords[12] = {0.0f, 0.0f,
                               1.0f, 0.0f,
                               1.0f, 1.0f,
                               0.0f, 0.0f,
                               1.0f, 1.0f,
                               0.0f, 1.0f };
    // tl->tr->br->tl->tr->bl
    GLfloat fa_vertex[12] = {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
                            0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};

    glBindMapTextureData(video_tex_, fa_vertex, fa_texcoords);
    glDrawArrays(GL_TRIANGLES, 0, 6); glCheckError();
    glUnbindMapTextureData();

  } else {  // UNDISTORT / DISTORT
    if (step1_display_) {
      glCallList(step1_display_);
    } else {
      step1_display_ = glGenLists(1);
      glNewList(step1_display_, GL_COMPILE);
        glUndistortTexture(video_tex_, undist_info_);
      glEndList();
    }
  }

  glDisable(GL_TEXTURE_2D);

  glClear(GL_DEPTH_BUFFER_BIT);  /// by default, set to 1.0f (far clipping plane)
}

template<typename CameraModel>
void ARNode<CameraModel>::MapTextureToFBO(const Rect& rect, GLuint fbo) {
  SaveSnapshot("test_fbo_texture");
  // map FBO Texture to default window-system-provided FBO
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(rect.x, rect.y, rect.width, rect.height);

  const Vec2f v2_offset = 0.5f*(undist_info_.undist_video_size - undist_info_.video_size);
//  glDisable(GL_POLYGON_SMOOTH);
  glDisable(GL_BLEND);
  glEnable(GL_TEXTURE_2D); glCheckError();
  if (render_mode_ == RenderMode::UNDISTORT) {
    // bl->tl->tr->bl->tr->br
    GLfloat fa_texcoords[12] = {
      v2_offset[0] / undist_info_.undist_video_size[0], v2_offset[1] / undist_info_.undist_video_size[1],
      v2_offset[0] / undist_info_.undist_video_size[0], (v2_offset[1] + undist_info_.video_size[1]) / undist_info_.undist_video_size[1],
      (v2_offset[0] + undist_info_.video_size[0]) / undist_info_.undist_video_size[0], (v2_offset[1] + undist_info_.video_size[1]) / undist_info_.undist_video_size[1],
      v2_offset[0] / undist_info_.undist_video_size[0], v2_offset[1] / undist_info_.undist_video_size[1],
      (v2_offset[0] + undist_info_.video_size[0]) / undist_info_.undist_video_size[0], (v2_offset[1] + undist_info_.video_size[1]) / undist_info_.undist_video_size[1],
      (v2_offset[0] + undist_info_.video_size[0]) / undist_info_.undist_video_size[0], v2_offset[1] / undist_info_.undist_video_size[1]};

    // bl->tl->tr->bl->tr->br
    GLfloat fa_vertex[12] = {0.0f, 0.0f, 0.0f, 1, 1, 1,
                             0.0f, 0.0f, 1, 1, 1, 0.0f};

    glBindMapTextureData(buffer_texture_, fa_vertex, fa_texcoords);
    glDrawArrays(GL_TRIANGLES, 0, 6); glCheckError();
    glUnbindMapTextureData();
  } else if (render_mode_ == RenderMode::DISTORT) {
    if (step2_display_) {
      glCallList(step2_display_);
    } else {
      step2_display_ = glGenLists(1);
      glNewList(step2_display_, GL_COMPILE);
      glDistortTexture(video_tex_, undist_info_);
      glEndList();
    }
  }

  glDisable(GL_TEXTURE_2D);
}

template<typename CameraModel>
bool ARNode<CameraModel>::GenerateFrameBuffers() {
  texture_initialized_ = true;
  // make texture function to be in GL_DECAL mode (replacing background colors)
  // http://glprogramming.com/red/chapter09.html
#ifndef GL_ES_VERSION_2_0
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
#endif
  // generate frame texture
  glGenTextures(1, &video_tex_);
  glBindTexture(GL_TEXTURE_2D, video_tex_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
               video_size_[0], video_size_[1], 0,
               vtexture_format_, GL_UNSIGNED_BYTE, NULL);

  // generate frame buffer texture
  glGenTextures(1, &buffer_texture_);
  glBindTexture(GL_TEXTURE_2D, buffer_texture_);
  glTexImage2D(GL_TEXTURE_2D, 0,
               GL_RGBA, buffer_size_[0], buffer_size_[1], 0,
               GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  // generate render buffer (depth buffer)
  glGenRenderbuffers(1, &buffer_depthbuf_);
  glBindRenderbuffer(GL_RENDERBUFFER, buffer_depthbuf_);
#if defined(GL_ES_VERSION_2_0)
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24_OES,
                        buffer_size_[0], buffer_size_[1]);
#else
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
                        buffer_size_[0], buffer_size_[1]);
#endif
  // generate frame buffer object
  glGenFramebuffers(1, &buffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                            GL_TEXTURE_2D, buffer_texture_, 0);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                               GL_RENDERBUFFER, buffer_depthbuf_);

  GLenum n = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (n == GL_FRAMEBUFFER_COMPLETE) {
    texture_initialized_ = true;
    return true;
  }
  printf("ARNode:: glCheckFrameBufferStatusExt returned an error.\n");
  return false;
}

// instantiate to trigger the rest =============================================
template ARNode<slick::PoliCamera<float>>::ARNode(
  const VecXf&, float, float, uint32_t id, PresentNode*);
template ARNode<slick::AtanCamera<float>>::ARNode(
  const VecXf&, float, float, uint32_t id, PresentNode*);
template ARNode<slick::PoliCamera<float>>::~ARNode();
template void ARNode<slick::PoliCamera<float>>::Init();
template void ARNode<slick::PoliCamera<float>>::Render();
template Eigen::Matrix4f ARNode<slick::PoliCamera<float>>::CalcProjectionMatrix(float, float);
template void ARNode<slick::PoliCamera<float>>::Configure();
}  // namespace present