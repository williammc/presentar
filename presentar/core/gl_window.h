// Copyright 2014 The PresentAR Authors. All rights reserved.
// GLWindow for handling OpenGL Window (GLUT) & events
#pragma once
#include "presentar/core/common_macros.h"
#include "presentar/core/datatypes.h"
#include "presentar/core/opengl.h"
#include "presentar/core/present_viewer.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
FORWARD_DECLARE_CLASS_SHARED_PTR(GLWindowMenu);

/// new glut app must derive from this class
class PRESENTAR_API GLWCallback {
 public:
  GLWCallback() {}
  virtual ~GLWCallback() {}
  virtual void OnDisplay() {}
  virtual void Keyboard(unsigned char c_key, int x, int y) {}
  virtual void HandleClick(int button, int state, int x, int y) {}

  PresentViewerPtr viewer() { return viewer_; }
 protected:
  PresentViewerPtr viewer_;
};

class SimpleGLWCallback : public GLWCallback {
 public:
  SimpleGLWCallback() {
    viewer_ = PresentViewerPtr(new PresentViewer());
    Mat4f proj = CalcProjectionMatrix(1, -1, 1, -1, 0.0f, 1.0f);
    viewer_->SetProjectionMatrix(proj);
  }

  virtual void OnDisplay(){
    viewer_->Update();
  }

};

// The main GLWinow class ======================================================
/// The main window class for glut
class PRESENTAR_API GLWindow {
 public:
  struct Properties {
    Properties() {
      glut_window_id = 0;
      window_width = 640;
      window_height = 480;
      init_pos_x = 100;
      init_pos_y = 100;
      window_name = "Put Window's Name Here";
      b_full_screen = false;  // fullscreen or not
      pausing = false;  // Pause or not
    }

    int glut_window_id;
    int window_width, window_height, init_pos_x, init_pos_y;
    std::string window_name;

    bool b_full_screen;  // fullscreen or not
    bool pausing;  // Pause or not
  };

  // Explicit default constructor
  GLWindow();

  GLWindow(GLWCallback* callback);

  // initialization for the GLWindow object
  void Init(const char* ca_name, int width, int height,
            int argc = 1, char** argv = NULL);

  void GLInit();

  /// the main loop
  void StartLoop();

  static void Update();
  static void ToggleFullscreen();

  // GLUT callbacks ============================================================
  // Render graphics (video frame & necessary objects)
  static void Display();

  // for glut keyboard handling
  static void Keyboard(unsigned char ckey, int x, int y);
  static void Idle();
  static void Reshape(const int w, const int h);
  static void MouseFunc(int button, int state, int x, int y);

  static void Release();
  static void Pause();

  static void SetCaption(std::string cap) {
    cur_caption_ = cap;
  }

  // Menu interface ============================================================
  void AddMenu(std::string name, std::string title);
  void AddMenu(GLWindowMenuPtr menu);
  static void DrawMenus();

  // Text display functions ====================================================
  static void PrintString(const Vec2f& loc, std::string s);
  static void DrawCaption(std::string s);

  static void SetupWindowOrtho();
  static void SetupViewport();
protected:
  friend class GLWindowMenu;

  static Properties properties_;
  static std::string cur_caption_;
  static int cur_width_, cur_height_;
  
  static std::vector<GLWindowMenuPtr> gl_menus_;  ///< interface menus
  static GLWCallback* callback_;
};

}  // namespace present