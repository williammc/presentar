// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once

// NOTE: OpenGL combined with Eigen needs to be initialized in this order.
// if you change the order, you might get linker errors!

#if defined(__APPLE__) || defined(MACOSX)
  #include <OpenGL/gl.h>
  #include <OpenGL/glext.h>
  #include <GLUT/glut.h>
#elif defined(WIN32)
// use GLEW => less painful
  #include <Windows.h>
  #include <GL/glew.h>  // this has to be before GL headers
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
  #include <GL/freeglut_ext.h>
  #include <GL/wglew.h>
#elif defined(ANDROID)
  #include <GLES2/gl2.h>
  #include <GLES2/gl2ext.h>
  #include <GLES2/gl2platform.h>
  #define FREEGLUT_GLES
  #include <GL/glut.h>
#elif defined(__linux)  // linux
  #include <GL/gl.h>
  #include <GL/glext.h>
  #include <GL/glut.h>
#else
    #error Unsupported platform
#endif