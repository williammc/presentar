// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "slick/scene/atan_camera.h"
#include "slick/scene/poli_camera.h"
#include "presentar/core/datatypes.h"
#include "presentar/core/opengl.h"
#include "presentar/core/present_viewer.h"
#include "presentar/util/undistortion.h"
#include "presentar/presentar_api.h"

#include <vector>

namespace present {
typedef slick::PoliCamera<float> PoliCam;
typedef slick::AtanCamera<float> AtanCam;
template<typename CameraModel> class ARNode;

typedef ARNode<PoliCam> ARNodePoliCam;
typedef std::shared_ptr<ARNodePoliCam> ARNodePoliCamPtr;

typedef ARNode<AtanCam> ARNodeAtanCam;
typedef std::shared_ptr<ARNodeAtanCam> ARNodeAtanCamPtr;

// AR rendering grand class, for distortion/undistortion rendering
template<typename CameraModel>
class ARNode : public PresentViewer {
 public :
  enum struct RenderMode : unsigned {UNDISTORT = 0, DISTORT = 1, ORIGINAL = 2};

  ARNode() = delete;
  ARNode(const ARNode&) = delete;
  ARNode(const VecXf& cam_params,
         float z_near = 0.0f, float z_far = 10.0f,
         uint32_t id = 0, PresentNode* p = nullptr);
  ~ARNode();

  void Init();
  void Render();
  void Update() {
    Render();
  }

  void SaveSnapshot(std::string filename);

  /// @return frustum_matrix in OpenGL coordinates
  Mat4f CalcProjectionMatrix(const float z_near = 0.0f,
                             const float z_far = 10.0f);

  const CameraModel& GetVideoCamera() const {
    return camera_;
  }

  /// only showing video background
  void SetNormalRender() {
    toset_render_mode_ = RenderMode::ORIGINAL;
  }

  /// distorted 3D
  void SetDistortionRender() {
    toset_render_mode_ = RenderMode::DISTORT;
  }

  /// undistorted video background
  void SetUndistortionRender() {
    toset_render_mode_ = RenderMode::UNDISTORT;
  }

  bool SetFramePixels(int width, int height, int bytedepth,
                      unsigned char* data, GLuint format = GL_RGB) {
    int nbytes = width*height*bytedepth;
    videoframe_pixels_.resize(nbytes);
    memcpy(&videoframe_pixels_.front(), data, nbytes);
    vtexture_format_ = format;
    return true;
  }

  bool SetFramePixels(std::vector<unsigned char>& pixels,
                      GLuint format = GL_RGB) {
    if (videoframe_pixels_.size() != pixels.size()) {
      return false;
    }
    videoframe_pixels_ = pixels;  // hopefully it's efficiently assigned ;)
    return true;
  }

  // convert coordinates from imageplane to current viewport coordinates
  Vec2f convert_coordinate(const Vec2f& point) {
    Vec2f p;
    p[0] = point[0]*width()/video_size_[0];
    p[1] = point[1]*height()/video_size_[1];
    return point;
  }

 protected :
  /// hide this so it's not misused
  void SetProjectionMatrix(const Mat4f& mat) {
    PresentViewer::SetProjectionMatrix(mat);
  }
  void Configure();
  // FrameTexture(binded to default FBO),
  // FrameBuffer, FrameBufferTexture (binded to FrameBuffer)
  bool GenerateFrameBuffers();
  // upload video frame to frame texture
  void UploadVideoFrame();
  // map rendered frame to default frame buffer
  void MapTextureToFBO(const Rect& rect, GLuint fbo = 0);

  // sampling step used for texture mapping (coordinate generation)
  const int texture_mapping_nsteps_;
  UndistortionInfo undist_info_;

  RenderMode render_mode_, toset_render_mode_;
  CameraModel camera_;
  std::vector<unsigned char> videoframe_pixels_;

  // texture stuffs
  GLuint video_tex_;
  GLuint vtexture_format_;  ///< e.x.: RGB, BGR, etc.
  Vec2f video_size_;

  // frame-buffer-object for AR rendering
  GLuint u_system_framebuffer;
  GLuint buffer_;
  GLuint buffer_texture_;
  GLuint buffer_depthbuf_;
  Vec2f buffer_size_;
  bool texture_initialized_;

  // display lists
  GLuint step1_display_, step2_display_;
};
}  // namespace present