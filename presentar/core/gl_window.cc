// Copyright 2014 The PresentAR Authors. All rights reserved.
// GLWindow for handling OpenGL Window (GLUT) & events
#pragma once
#include <iostream>
#include "slick/scene/poli_camera.h"
#include "presentar/core/common_macros.h"
#include "presentar/core/opengl.h"
#include "presentar/core/ar_node.h"
#include "presentar/core/gl_window_menu.h"
#include "presentar/core/present_node.h"
#include "presentar/util/gl_draw_helpers.h"
#include "presentar/presentar_api.h"

namespace present {
typedef ARNode<slick::PoliCamera<float>> ARNodePoliType;
typedef ARNode<slick::AtanCamera<float>> ARNodeAtanType;

  // static members
std::string GLWindow::cur_caption_;
int GLWindow::cur_width_;
int GLWindow::cur_height_;
GLWindow::Properties GLWindow::properties_;
GLWCallback* GLWindow::callback_;
std::vector<GLWindowMenuPtr> GLWindow::gl_menus_;

// Explicit default constructor
GLWindow::GLWindow() {}

GLWindow::GLWindow(GLWCallback* callback) {
  callback_ = callback;
}

// initialization for the GLWindow object
void GLWindow::Init(const char* ca_name, int width, int height,
          int argc, char** argv) {
  properties_.window_width = width;
  properties_.window_height = height;
  properties_.b_full_screen = true;
  properties_.window_name = ca_name;

  cur_caption_ = "";
  cur_width_ = width;
  cur_height_ = height;
  if (callback_)
    callback_->viewer()->ResizeWindow(width, height);

  // OpenGL stuff
  glutInit(&argc, argv);  // using glut for window
  GLInit();               // opengl (& glut) initialization
#ifdef WIN32
  if (GLEW_OK != glewInit()) {
    std::cerr << "GLEW init failed";
    exit(1);
  }
#endif
}

void GLWindow::GLInit() {
  // glut window initialisation
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowPosition(properties_.init_pos_x, properties_.init_pos_y);
  glutInitWindowSize(properties_.window_width, properties_.window_height);
  properties_.glut_window_id =
      glutCreateWindow(properties_.window_name.c_str());
}

/// the main loop
void GLWindow::StartLoop() {
  // glut callbacks setting
  glutReshapeFunc(Reshape);
  glutIdleFunc(Idle);
  glutDisplayFunc(Display);
  glutKeyboardFunc(Keyboard);
  glutMouseFunc(MouseFunc);

  //glutFullScreen();
  glutMainLoop();  // for glut
}

void GLWindow::Update() { glutSwapBuffers(); }

void GLWindow::ToggleFullscreen() {
  if (properties_.b_full_screen) {
    glutPositionWindow(properties_.init_pos_x, properties_.init_pos_y);
    glutReshapeWindow(properties_.window_width, properties_.window_height);
    if (callback_->viewer()) {
      auto render = dynamic_cast<ARNodePoliType*>(callback_->viewer().get());
      if (render) {
        render->ResizeWindow(properties_.window_width,
                     properties_.window_height);
      } else {
        auto render1 = dynamic_cast<ARNodeAtanType*>(callback_->viewer().get());
        if (render1)
          render1->ResizeWindow(properties_.window_width,
                               properties_.window_height);
      }
    }
  } else {
    glutFullScreen();
  }
  properties_.b_full_screen = !properties_.b_full_screen;
}

// GLUT callbacks ============================================================
// Render graphics (video frame & necessary objects)
void GLWindow::Display() {
  callback_->OnDisplay();
  DrawCaption(cur_caption_);
  DrawMenus();
  Update();
}

// for glut keyboard handling
void GLWindow::Keyboard(unsigned char ckey, int x, int y) {
  callback_->Keyboard(ckey, x, y);
  switch (ckey) {
  case 0x1b:  // ESC
#ifdef WIN32
      glutLeaveMainLoop();
#else
    Release();
#endif
    break;
  case 'w':
    GLWindow::ToggleFullscreen();  // full screen or normal screen
    break;
  case 'p':
    Pause();
    break;
  }
}

void GLWindow::Idle() {
  glutPostRedisplay();
}

void GLWindow::Reshape(const int w, const int h) {
  if (callback_->viewer())
    callback_->viewer()->ResizeWindow(w, h);
  Update();
}

void GLWindow::MouseFunc(int button, int state, int x, int y) {
  bool handled = false;
  for (unsigned int i = 0; !handled && i < gl_menus_.size(); i++)
    handled = gl_menus_[i]->HandleClick(button, state, x, y);

  callback_->HandleClick(button, state, x, y);
}

void GLWindow::Release() {
  // TODO: proper release
  exit(1);
}

void GLWindow::Pause() {
  if (!properties_.pausing) {
    glutDisplayFunc(GLWindow::Display);
  } else {
    glutDisplayFunc(GLWindow::Idle);
  }
  properties_.pausing = !properties_.pausing;
}

// Menu interface ==============================================================
void GLWindow::AddMenu(std::string name, std::string title) {
  gl_menus_.push_back(GLWindowMenuPtr(new GLWindowMenu(name, title)));
}

void GLWindow::AddMenu(GLWindowMenuPtr menu) {
  gl_menus_.push_back(menu);
}

void GLWindow::DrawMenus() {
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_TEXTURE_RECTANGLE_ARB);
  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_POLYGON_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glColorMask(1, 1, 1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  SetupWindowOrtho();
  glLineWidth(1);

  int top = 30;
  int height = 30;
  for (auto& menu : gl_menus_) {
    menu->Render(top, height, cur_width_);
    top += height + 1;
  }
}

// Text display functions:
void GLWindow::PrintString(const Vec2f& loc, std::string s) {
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glTranslatef(loc[0], loc[1], 0.0);
  glScalef(8, -8, 1);
  glDrawText(s);
  glPopMatrix();
}

void GLWindow::DrawCaption(std::string s) {
  if (s.length() == 0)
    return;

  SetupWindowOrtho();
  // Find out how many lines are in the caption:
  // Count the endls
  int nlines = 0;
  {
    std::string sendl("\n");
    std::string::size_type st = 0;
    while (1) {
      nlines++;
      st = s.find(sendl, st);
      if (st == std::string::npos)
        break;
      else
        st++;
    }
  }

  int topofbox = cur_height_ - nlines * 17;

  // Draw a grey background box for the text
  glColor4f(0, 0, 0, 0.4);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBegin(GL_QUADS);
  glVertex2d(-0.5, topofbox);
  glVertex2d(cur_width_, topofbox);
  glVertex2d(cur_width_, cur_height_);
  glVertex2d(-0.5, cur_height_);
  glEnd();

  // Draw the caption text in yellow
  glColor3f(1, 1, 0);
  PrintString(Vec2f(10, topofbox + 13), s);
}

void GLWindow::SetupWindowOrtho() {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, cur_width_, cur_height_, 0, -1, 1);
}

void GLWindow::SetupViewport() {
  glViewport(0, 0, cur_width_, cur_height_);
}
}  // namespace present