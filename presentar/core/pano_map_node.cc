#include "presentar/core/pano_map_node.h"
#include "slick/util/common.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/util/png_io.h"

using std::vector;

namespace present {

// constructor must call the base class constructor.
PanoMapNode::PanoMapNode(Rect pano_rect, const VecXf& cam_params, const float height,
                 uint32_t id, PresentNode* parent)
  : FboNode(Vec2f(0, 0), id, parent),
    pano_rect_(pano_rect),
    need_reset_panorama_(true), update_panorama_(true),
    texture_initialized_(false),
    camera_(cam_params) {
  highlight_color_ << 0, 0, 1;
  Configure(height);
}

PanoMapNode::~PanoMapNode() {
  glDeleteTextures(1, &frame_tex_);
}

void PanoMapNode::Configure(float height_angle) {
  int w, h;
  camera_.get_resolution(w, h);
  video_size_ = Vec2f(w, h);
  videoframe_pixels_.resize(video_size_[0]*video_size_[1]*3);
  // work out Panorama Buffer size to cover 360 degree
  // work out camera field-of-views
  const Vec3f ml =
      (slick::unproject(
         camera_.UnProject(
           Vec2f(0, video_size_[1]/2)))).normalized();
  const Vec3f mr =
      (slick::unproject(
         camera_.UnProject(
           Vec2f(video_size_[0],
                           video_size_[1]/2)))).normalized();
  const float hori_fov = std::acos(ml.transpose()*mr);
  const Vec3f mt =
      (slick::unproject(
         camera_.UnProject(
           Vec2f(video_size_[0]/2, 0)))).normalized();
  const Vec3f mb =
      (slick::unproject(
         camera_.UnProject(
           Vec2f(video_size_[0]/2,
                           video_size_[1])))).normalized();
  const float ver_fov = std::acos(mt.transpose()*mb);

  // work out width/height of panorama buffer
  printf("hori_fov:%f ver_fov:%f vertical ratio:\n",
         hori_fov*180/M_PI, ver_fov*180/M_PI, height_angle*M_PI/180/ver_fov);
  buffer_size_[0] = (2*M_PI/hori_fov)*video_size_[0];
  buffer_size_[1] = (height_angle*M_PI/180/ver_fov)*video_size_[1];
  cylinder_ = slick::Cylinder<float>(buffer_size_, height_angle);
}

void PanoMapNode::Init() {
  if (initialized_) return;
}

void PanoMapNode::Render() {
  if (!texture_initialized_) {
    GenerateFrameBuffers();
  }

  UploadVideoFrame();
  MapTextureToFBO(pano_rect_, 0);
  HighlightCurrentFrame();

  for (auto& child : children_) child->Update();

  //SaveSnapshot("pano.png", "pano1.png");
}

void PanoMapNode::ResizeWindow(Rect rect) {
  // setup viewport
  pano_rect_ = rect;
}

void PanoMapNode::SaveSnapshot(std::string filename,
                              std::string withmeas_filename) const {
  glBindFramebuffer(GL_FRAMEBUFFER, buffer_);

  std::vector<unsigned char> v_pixels(buffer_size_[0]*buffer_size_[1]*3);
  glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glReadPixels(0, 0,
               buffer_size_[0], buffer_size_[1],
               GL_BGR, GL_UNSIGNED_BYTE, &v_pixels.front());

  bitmap_t bitmap{ v_pixels, buffer_size_[0], buffer_size_[1], 3, 8 };
  write_png_file(bitmap, (filename + "-0.png").c_str());

  glClear(GL_DEPTH_BUFFER_BIT);
  glViewport(0, 0, buffer_size_[0], buffer_size_[1]);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, buffer_size_[0], 0, buffer_size_[1], 0, -1);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_DEPTH_TEST);
  
  for (auto& child : children_) child->Update();

  glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glReadPixels(0, 0,
               buffer_size_[0], buffer_size_[1],
               GL_BGR, GL_UNSIGNED_BYTE, &v_pixels.front());

  bitmap = bitmap_t{v_pixels, buffer_size_[0], buffer_size_[1], 3, 8 };
  write_png_file(bitmap, (filename + "-1.png").c_str());

  glClear(GL_DEPTH_BUFFER_BIT);
}

inline bool is_in_pairs(const std::pair<unsigned, unsigned>& p,
                        const std::vector<std::pair<unsigned, unsigned> >& ps) {
  for (unsigned i = 0; i < ps.size(); ++i) {
    if ((p.first == ps[i].first) && (p.second == ps[i].second))
      return  true;
  }
  return false;
}

void PanoMapNode::UploadVideoFrame() {
  std::vector<unsigned char>& v_pixels = videoframe_pixels_;

  const Mat3f current_w2c = world2cam_pose_.block<3, 3>(0, 0);

  if (update_panorama_) {
    // uploads the image frame to the frame texture
    glBindTexture(GL_TEXTURE_2D, frame_tex_); glCheckError();
    glTexSubImage2D(GL_TEXTURE_2D,
                    0, 0, 0,
                    video_size_[0], video_size_[1],
                    GL_RGB,
                    GL_UNSIGNED_BYTE,
                    &v_pixels.front()); glCheckError();
    update_panorama_ = false;
    // work out texture coordinates mapping from frametexture to panorama region
    std::vector<Vec2f> v_texture_coors, v_vertex_coors;
    const int nsteps = 24;
    std::vector<std::pair<unsigned, unsigned> > crossing_pairs;
    Vec2f v2_imageplane, v2_cylinderplane, last_v2_cylinderplane;
    for (unsigned y = 0; y < nsteps; ++y) {
      for (unsigned x = 0; x < nsteps; ++x) {
        v2_imageplane[0] = static_cast<float>(x*video_size_[0])/(nsteps-1);
        v2_imageplane[1] = static_cast<float>(y*video_size_[1])/(nsteps-1);
        if (x == 0 || y == 0) {
          v2_imageplane[0] = v2_imageplane[0] + 3;
          v2_imageplane[1] = v2_imageplane[1] + 3;
        } else if (x == nsteps-1 || y == nsteps-1) {
          v2_imageplane[0] = v2_imageplane[0] - 3;
          v2_imageplane[1] = v2_imageplane[1] - 3;
        }

        v2_cylinderplane =
            MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
        
        v_texture_coors.push_back(Vec2f(v2_imageplane[0] / video_size_[0],
                                                  v2_imageplane[1] / video_size_[1]));
        v_vertex_coors.push_back(Vec2f(v2_cylinderplane[0],
                              v2_cylinderplane[1]));

        if (x == 0) {
          last_v2_cylinderplane = v2_cylinderplane;
          continue;
        }
        if ((std::fabs(last_v2_cylinderplane[0]-v2_cylinderplane[0]) > buffer_size_[0]/2) ||
            (std::fabs(last_v2_cylinderplane[1]-v2_cylinderplane[1]) > buffer_size_[1]/2))
          crossing_pairs.push_back(std::make_pair(x, y));
        last_v2_cylinderplane = v2_cylinderplane;
      }
    }
    for (unsigned x = 0; x < nsteps; ++x) {
      for (unsigned y = 0; y < nsteps; ++y) {
        v2_imageplane[0] = static_cast<float>(x*video_size_[0])/(nsteps-1);
        v2_imageplane[1] = static_cast<float>(y*video_size_[1])/(nsteps-1);
        v2_cylinderplane =
          MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
        if (y == 0) {
          last_v2_cylinderplane = v2_cylinderplane;
          continue;
        }
        if ((std::fabs(last_v2_cylinderplane[0]-v2_cylinderplane[0]) > buffer_size_[0]/2) ||
            (std::fabs(last_v2_cylinderplane[1]-v2_cylinderplane[1]) > buffer_size_[1]/2))
          crossing_pairs.push_back(std::make_pair(x, y));
        last_v2_cylinderplane = v2_cylinderplane;
      }
    }

    // map FBO Texture to default window-system-provided FBO
    glBindFramebuffer(GL_FRAMEBUFFER, buffer_); glCheckError();
    glClear(GL_DEPTH_BUFFER_BIT);
    if (need_reset_panorama_) {
      glClear(GL_COLOR_BUFFER_BIT);
      glClearColor(0, 0, 0, 1.f); glCheckError();
      need_reset_panorama_ = false;
    }

    glViewport(0, 0, buffer_size_[0], buffer_size_[1]); glCheckError();

    glMatrixMode(GL_PROJECTION); glCheckError();
    glLoadIdentity(); glCheckError();
    glOrtho(0, buffer_size_[0], 0, buffer_size_[1], 0, 1); glCheckError();

    glMatrixMode(GL_MODELVIEW); glCheckError();
    glLoadIdentity(); glCheckError();

    glEnable(GL_TEXTURE_2D); glCheckError();
    glBindTexture(GL_TEXTURE_2D, frame_tex_); glCheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); glCheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); glCheckError();
    glDisable(GL_POLYGON_SMOOTH); glCheckError();
    glDisable(GL_BLEND); glCheckError();

    glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
    glEnableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();
    glTexCoordPointer(2, GL_FLOAT,
                      sizeof(Vec2f), &v_texture_coors.front()[0]); glCheckError();
    glVertexPointer(2, GL_FLOAT,
                    sizeof(Vec2f), &v_vertex_coors.front()[0]); glCheckError();

    for (unsigned y = 0; y < nsteps-1; ++y) {
      std::vector<unsigned> elements;
      for (unsigned x = 0; x < nsteps; ++x) {
        if (is_in_pairs(std::make_pair(x, y), crossing_pairs) ||
            is_in_pairs(std::make_pair(x, y+1), crossing_pairs)) {
          glDrawElements(GL_QUAD_STRIP, elements.size(), GL_UNSIGNED_INT, &elements.front()); glCheckError();
          elements.clear();
          continue;
        }
        elements.push_back(x+(y+1)*nsteps);
        elements.push_back(x+(y)*nsteps);
      }
      glDrawElements(GL_QUAD_STRIP, elements.size(), GL_UNSIGNED_INT, &elements.front()); glCheckError();
    }
    glDisableClientState(GL_VERTEX_ARRAY); glCheckError();
    glDisableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();

    glDisable(GL_TEXTURE_2D); glCheckError();
  }

#if 0  // for testing purpose
  std::cout << "PanoMapNode:: current pose \n" << se3_current << std::endl;
  static int count = 0;
  vector<unsigned char> v_pixels_temp(buffer_size_[0]*buffer_size_[1]*3);
  glBindFramebuffer(GL_FRAMEBUFFER, u_buffer_);
  glReadPixels(0, 0,
               buffer_size_[0], buffer_size_[1],
               GL_BGR, GL_UNSIGNED_BYTE, &v_pixels_temp.front());
  cv::Mat m_temp(buffer_size_[1], buffer_size_[0],
      CV_8UC3, &v_pixels_temp.front());
  cv::flip(m_temp, m_temp, 0);
  cv::imwrite("test_panorama.tiff", m_temp);
#endif
}

void PanoMapNode::HighlightCurrentFrame() {
  glMatrixMode(GL_PROJECTION); glCheckError();
  glPushMatrix();
  glLoadIdentity(); glCheckError();
  // l, r, t, b, near, far
  glOrtho(0.0, 1.0, 1.0, 0.0, 0.0, 1.0);  // Vision Coordiates (start top-left)

  glMatrixMode(GL_MODELVIEW); glCheckError();
  glPushMatrix();
  glLoadIdentity(); glCheckError();

  // highlight current view
  const Mat3f current_w2c = world2cam_pose_.block<3, 3>(0, 0);

  // Anti-aliased lines
  glDisable(GL_LIGHTING); glCheckError();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); glCheckError();
  glEnable(GL_BLEND); glCheckError();
  glDisable(GL_DEPTH_TEST); glCheckError();
  glEnable(GL_LINE_SMOOTH); glCheckError();

  glLineWidth(2.0); glCheckError();
  glColor3d(highlight_color_[0], highlight_color_[1], highlight_color_[2]); glCheckError();

  Vec2f v2_imageplane, v2_cylinderplane, last_v2_cylinderplane;
  const unsigned nsteps = 50;
  std::vector<Vec2f> lines;
  // top lines
  last_v2_cylinderplane =
    MapCurrentPixelToPanorama(Vec2f(0, 0), current_w2c);
  last_v2_cylinderplane[0] /= buffer_size_[0];
  last_v2_cylinderplane[1] /= buffer_size_[1];
  for (unsigned x = 1; x <= nsteps; ++x) {
    v2_imageplane[0] = static_cast<float>(x*video_size_[0]) / nsteps;
    v2_imageplane[1] = static_cast<float>(0 * video_size_[1]) / nsteps;
    v2_cylinderplane =
      MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
    v2_cylinderplane[0] /= buffer_size_[0];
    v2_cylinderplane[1] /= buffer_size_[1];
    if (!(std::fabs(last_v2_cylinderplane[0] - v2_cylinderplane[0]) > 0.5) &&
        !(std::fabs(last_v2_cylinderplane[1] - v2_cylinderplane[1]) > 0.5)) {
      lines.push_back(last_v2_cylinderplane);
      lines.push_back(v2_cylinderplane);
    }
    last_v2_cylinderplane = v2_cylinderplane;
  }

  // right lines
  last_v2_cylinderplane =
    MapCurrentPixelToPanorama(Vec2f(video_size_[0], 0), current_w2c);
  last_v2_cylinderplane[0] /= buffer_size_[0];
  last_v2_cylinderplane[1] /= buffer_size_[1];
  for (unsigned y = 1; y <= nsteps; ++y) {
    v2_imageplane[0] = static_cast<float>(video_size_[0]);
    v2_imageplane[1] = static_cast<float>(y*video_size_[1]) / nsteps;
    v2_cylinderplane =
      MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
    v2_cylinderplane[0] /= buffer_size_[0];
    v2_cylinderplane[1] /= buffer_size_[1];
    if (!(std::fabs(last_v2_cylinderplane[0] - v2_cylinderplane[0]) > 0.5) &&
        !(std::fabs(last_v2_cylinderplane[1] - v2_cylinderplane[1]) > 0.5)) {
      lines.push_back(last_v2_cylinderplane);
      lines.push_back(v2_cylinderplane);
    }
    last_v2_cylinderplane = v2_cylinderplane;
  }
  // bottom lines
  last_v2_cylinderplane =
    MapCurrentPixelToPanorama(Vec2f(video_size_[0], video_size_[1]), current_w2c);
  last_v2_cylinderplane[0] /= buffer_size_[0];
  last_v2_cylinderplane[1] /= buffer_size_[1];
  for (int x = nsteps - 1; x >= 0; --x) {
    v2_imageplane[0] = static_cast<float>(x*video_size_[0]) / nsteps;
    v2_imageplane[1] = static_cast<float>(video_size_[1]);
    v2_cylinderplane =
      MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
    v2_cylinderplane[0] /= buffer_size_[0];
    v2_cylinderplane[1] /= buffer_size_[1];
    if (!(std::fabs(last_v2_cylinderplane[0] - v2_cylinderplane[0]) > 0.5) &&
        !(std::fabs(last_v2_cylinderplane[1] - v2_cylinderplane[1]) > 0.5)) {
      lines.push_back(last_v2_cylinderplane);
      lines.push_back(v2_cylinderplane);
    }
    last_v2_cylinderplane = v2_cylinderplane;
  }

  // left lines
  last_v2_cylinderplane =
    MapCurrentPixelToPanorama(Vec2f(0, video_size_[1]), current_w2c);
  last_v2_cylinderplane[0] /= buffer_size_[0];
  last_v2_cylinderplane[1] /= buffer_size_[1];
  for (int y = nsteps - 1; y >= 0; --y) {
    v2_imageplane[0] = 0;
    v2_imageplane[1] = static_cast<float>(y*video_size_[1]) / nsteps;
    v2_cylinderplane =
      MapCurrentPixelToPanorama(v2_imageplane, current_w2c);
    v2_cylinderplane[0] /= buffer_size_[0];
    v2_cylinderplane[1] /= buffer_size_[1];
    if (!(std::fabs(last_v2_cylinderplane[0] - v2_cylinderplane[0]) > 0.5) &&
        !(std::fabs(last_v2_cylinderplane[1] - v2_cylinderplane[1]) > 0.5)) {
      lines.push_back(last_v2_cylinderplane);
      lines.push_back(v2_cylinderplane);
    }
    last_v2_cylinderplane = v2_cylinderplane;
  }

  glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
  glVertexPointer(2, GL_FLOAT, sizeof(Vec2f), &lines.front()[0]); glCheckError();
  glDrawArrays(GL_LINES, 0, lines.size()); glCheckError();
  glDisableClientState(GL_VERTEX_ARRAY); glCheckError();


  glMatrixMode(GL_PROJECTION); glCheckError();
  glPopMatrix();

  glMatrixMode(GL_MODELVIEW); glCheckError();
  glPopMatrix();
}

// map current pixel coordinate to panorama coordinate
Vec2f PanoMapNode::MapCurrentPixelToPanorama(
    Vec2f const& v2_implane, const Mat3f& w2c) const {
  Vec3f v3_camplane =
      slick::unproject(camera_.UnProject(v2_implane));
  v3_camplane = (w2c.inverse()*v3_camplane).normalized();

  return cylinder_.project(v3_camplane);
}

// map 3D point in world coordinate to panorama coordinate
Vec2f PanoMapNode::Map3DPointToUnitPanorama(
    const Vec3f& v3_worldpoint) const {
  Vec3f v3_camplane = (v3_worldpoint/v3_worldpoint[2]).normalized();
  return cylinder_.project(v3_camplane).cwiseQuotient(cylinder_.size());
}

// map 3D point in world coordinate to panorama coordinate
Vec2f PanoMapNode::Map3DPointToPanorama(
    const Vec3f& v3_worldpoint) const {
  Vec3f v3_camplane = (v3_worldpoint/v3_worldpoint[2]).normalized();
  return cylinder_.project(v3_camplane);
}

bool PanoMapNode::GenerateFrameBuffers() {
  texture_initialized_ = true;
  // make texture function to be in GL_DECAL mode (replacing background colors)
  // http://glprogramming.com/red/chapter09.html
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL); glCheckError();

  // generate frame texture
  glGenTextures(1, &frame_tex_); glCheckError();
  glBindTexture(GL_TEXTURE_2D, frame_tex_); glCheckError();
  glTexImage2D(GL_TEXTURE_2D, 0,
               GL_RGBA, video_size_[0], video_size_[1], 0,
               GL_RGBA, GL_UNSIGNED_BYTE, NULL); glCheckError();

  GLenum n;
  n = glCheckFramebufferStatus(GL_FRAMEBUFFER); glCheckError();
  if (n != GL_FRAMEBUFFER_COMPLETE_EXT) {

    printf("glCheckFrameBufferStatusExt returned an error.\n");
    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, buffer_); glCheckError();
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT); glCheckError();
  return true;  // ok
}


//void PanoMapNode::UploadTextureToPanorama(cv::Mat& texture) {
//
//  printf("texture (%d, %d)\n", texture.cols, texture.rows);
////  // generate frame texture
////  GLuint u_frametexture_temp;
////  glGenTextures(1, &u_frametexture_temp);
////  glBindTexture(GL_TEXTURE_2D, buffer_texture_);
////  glTexImage2D(GL_TEXTURE_2D, 0,
////               GL_RGBA, texture.cols, texture.rows, 0,
////               GL_RGBA, GL_UNSIGNED_BYTE, texture.data);
//
//  // uploads the image frame to the frame texture
//  glBindTexture(GL_TEXTURE_2D, buffer_texture_);
//  glTexSubImage2D(GL_TEXTURE_2D,
//                  0, 0, 0,
//                  texture.cols, texture.rows,
//                  GL_RGB,
//                  GL_UNSIGNED_BYTE,
//                  texture.data);
//
////  cv::imwrite("texture.jpg", texture);
//
////  // map frame texture to FBO / window-default-provided FBO
////  // binding to window-system-provided FrameBuffer
////  glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
////  glViewport(0, 0, buffer_size_[0], buffer_size_[1]);
////  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
////  glMatrixMode(GL_PROJECTION);
////  glLoadIdentity();
////  glOrtho(0, 1, 0, 1, 0, 1);  // OpenGL coordinates (start at bottom-left)
//
////  glMatrixMode(GL_MODELVIEW);
////  glLoadIdentity();
//
////  glEnable(GL_TEXTURE_2D);
////  glBindTexture(GL_TEXTURE_2D, u_frametexture_temp);
////  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
////  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
////  glDisable(GL_POLYGON_SMOOTH);
////  glDisable(GL_BLEND);
////  glEnableClientState(GL_VERTEX_ARRAY);
////  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
////  GLfloat fa_texcoords[8] = {0, 0,
////                             texture.cols, 0,
////                             texture.cols, texture.rows,
////                             0, texture.rows};
////  //    GLfloat fa_vertex[8] = {0, 0, 1, 0, 1, 1, 0, 1}; original
////  GLfloat fa_vertex[8] = {0, 1, 1, 1, 1, 0, 0, 0};
////  glTexCoordPointer(2, GL_FLOAT, 0, fa_texcoords);
////  glVertexPointer(2, GL_FLOAT, 0, fa_vertex);
////  glDrawArrays(GL_QUADS, 0, 4);
////  glDisableClientState(GL_VERTEX_ARRAY);
////  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
////  glDisable(GL_TEXTURE_2D);
//}
}  // namespace present
