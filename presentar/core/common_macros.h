// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <memory>

#define FORWARD_DECLARE_STRUCT_SHARED_PTR(T)          \
struct T;                                             \
using T##Ptr = std::shared_ptr<T>;                    \


#define FORWARD_DECLARE_CLASS_SHARED_PTR(T)          \
class T;                                             \
using T##Ptr = std::shared_ptr<T>;                   \


