// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <memory>
#include "presentar/core/datatypes.h"
#include "presentar/core/opengl.h"
#include "presentar/core/present_node.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/util/png_io.h"
#include "presentar/presentar_api.h"

namespace present {
/// A Frame buffer object render node
class PRESENTAR_API FboNode : public PresentNode {
 public:
   FboNode(const Vec2f& size, uint32_t id = 0, PresentNode* parent = nullptr)
     : PresentNode(id, parent), buffer_size_(size) {}

   ~FboNode() {
     glDeleteBuffers(1, &buffer_);
     glDeleteTextures(1, &buffer_texture_);
   }

  virtual void Init() {
    if (initialized_) return;
    GenerateFrameBuffers();
    initialized_ = true;
  }
  virtual void Update() {
    DoBeforeRender();

    for (auto child : children_) child->Update();

    DoAfterRender();
  }
  virtual void Render() {}

  void MapTextureToFBO(const Rect& rect, GLuint fbo = 0) {
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); glCheckError();
    glClear(GL_DEPTH_BUFFER_BIT); glCheckError();
    glDisable(GL_DEPTH_TEST); glCheckError();
    glViewport(rect.x, rect.y, rect.width, rect.height); glCheckError();

    glMatrixMode(GL_PROJECTION); glCheckError();
    glPushMatrix();
    glLoadIdentity(); glCheckError();
    // l, r, b, t, near, far
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);  // OpenGL coordinates (start at bottom-left)

    glMatrixMode(GL_MODELVIEW); glCheckError();
    glPushMatrix();
    glLoadIdentity(); glCheckError();

    glEnable(GL_TEXTURE_2D); glCheckError();
    glBindTexture(GL_TEXTURE_2D, buffer_texture_); glCheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); glCheckError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); glCheckError();

    glEnableClientState(GL_VERTEX_ARRAY); glCheckError();
    glEnableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();

    /// bl->br->tr->tl
    std::array<GLfloat, 8> fa_texcoords = { 0.0f, 0.0f,
      1.0f, 0.0f,
      1.0f, 1.0f,
      0.0f, 1.0f };
    std::array<GLfloat, 8> fa_vertex = fa_texcoords;
    if (true) {
      /// tl->tr->br->bl (reversed order)
      fa_vertex = std::array<GLfloat, 8>{0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f};
    }

    glTexCoordPointer(2, GL_FLOAT, 0, &fa_texcoords.front()); glCheckError();
    glVertexPointer(2, GL_FLOAT, 0, &fa_vertex.front()); glCheckError();

    glDrawArrays(GL_QUADS, 0, 4); glCheckError();

    glDisableClientState(GL_TEXTURE_COORD_ARRAY); glCheckError();
    glDisableClientState(GL_VERTEX_ARRAY); glCheckError();

    glDisable(GL_TEXTURE_2D); glCheckError();

    glMatrixMode(GL_PROJECTION); glCheckError();
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW); glCheckError();
    glPopMatrix();
  }

  Vec2f size() { return buffer_size_; }
  void SetSize(const Vec2f& size) {
    buffer_size_ = size;
  }

  GLuint buffer() { return buffer_; }
  GLuint texture() { return buffer_texture_; }

  void SaveSnapshot(std::string filename) const {
    glBindFramebuffer(GL_FRAMEBUFFER, buffer_);

    std::vector<unsigned char> v_pixels(buffer_size_[0] * buffer_size_[1] * 3);
    glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadPixels(0, 0,
                 buffer_size_[0], buffer_size_[1],
                 GL_BGR, GL_UNSIGNED_BYTE, &v_pixels.front());
    bitmap_t bitmap{ v_pixels, buffer_size_[0], buffer_size_[1], 3, 8 };
    filename += ".png";
    write_png_file(bitmap, filename.c_str());
  }

 protected:
   virtual void DoBeforeRender() {
     Init();
     glBindFramebuffer(GL_FRAMEBUFFER, buffer_);
     glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT); glCheckError();
     glViewport(0, 0, buffer_size_[0], buffer_size_[1]);
   }
   virtual void DoAfterRender() {
     glBindFramebuffer(GL_FRAMEBUFFER, 0);
   }
   bool GenerateFrameBuffers() {
     texture_initialized_ = true;
     // make texture function to be in GL_DECAL mode (replacing background colors)
     // http://glprogramming.com/red/chapter09.html
     glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL); glCheckError();

     // generate panorama buffer texture
     glGenTextures(1, &buffer_texture_); glCheckError();
     glBindTexture(GL_TEXTURE_2D, buffer_texture_); glCheckError();
     glTexImage2D(GL_TEXTURE_2D, 0,
                  GL_RGBA, buffer_size_[0], buffer_size_[1], 0,
                  GL_RGBA, GL_UNSIGNED_BYTE, NULL); glCheckError();
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); glCheckError();
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); glCheckError();

     // generate render buffer (depth buffer)
     glGenRenderbuffers(1, &fbo_depthbuffer_); glCheckError();
     glBindRenderbuffer(GL_RENDERBUFFER, fbo_depthbuffer_); glCheckError();
     glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
                           buffer_size_[0], buffer_size_[1]); glCheckError();

     // generate frame buffer object
     glGenFramebuffers(1, &buffer_); glCheckError();
     glBindFramebuffer(GL_FRAMEBUFFER, buffer_); glCheckError();
     glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0_EXT,
                            GL_TEXTURE_2D,
                            buffer_texture_, 0); glCheckError();
     glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT_EXT,
                               GL_RENDERBUFFER_EXT, fbo_depthbuffer_); glCheckError();

     GLenum n;
     n = glCheckFramebufferStatus(GL_FRAMEBUFFER); glCheckError();
     if (n != GL_FRAMEBUFFER_COMPLETE_EXT) {

       printf("glCheckFrameBufferStatusExt returned an error.\n");
       return false;
     }

     glBindFramebuffer(GL_FRAMEBUFFER, buffer_); glCheckError();
     glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT); glCheckError();
     return true;  // ok
   }

  // Properties ---------------------------------------------------------------
  bool save_snapshot_;

  // FBO stuffs
  Vec2f buffer_size_;
  GLuint buffer_;
  GLuint buffer_texture_;
  GLuint fbo_depthbuffer_;

  bool texture_initialized_;
};
typedef std::shared_ptr<FboNode> FboNodePtr;
}  // namespace present