#pragma once
#include "presentar/core/prensent_node.h"
#include "presentar/util/gl_helpers.h"

namespace present {
class TextureNode;
using TextureNodePtr = std::shared_ptr<TextureNode>;

class TextureNode : public PresentNode {
 public:
  TextureNode(const TextureNode&) = delete;
  TextureNode(const Vec2f& tex_size, uint32_t id = 0, PresentNode* p) 
    : PresentNode(id, p), tex_size_(tex_size) {}
  ~TextureNode() {
    glDeleteTextures(1, &tex_);
  }

  void Init() {
    if (initialized_) return;
  // make texture function to be in GL_DECAL mode (replacing background colors)
    // http://glprogramming.com/red/chapter09.html
  #ifndef GL_ES_VERSION_2_0
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  #endif
    // generate frame texture
    glGenTextures(1, &tex_);
    glBindTexture(GL_TEXTURE_2D, tex_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                 tex_size_[0], tex_size_[1], 0,
                 tex_format_, GL_UNSIGNED_BYTE, NULL);

    initialized_ = true;
  }

  void Render() {
    assert(!tex_pixels_.empty());
    // uploads the image frame to the frame texture
    glBindTexture(GL_TEXTURE_2D, tex_); glCheckError();
    glTexSubImage2D(GL_TEXTURE_2D,
                    0, 0, 0,
                    video_size_[0], video_size_[1],
                    vtexture_format_,
                    GL_UNSIGNED_BYTE,
                    &pixels.front()); glCheckError();

    glDisable(GL_BLEND);
    glEnable(GL_TEXTURE_2D); glCheckError();

    // draw orders = 0, 1, 2, 0, 2, 3
    // bl->br->tr->bl->tr->tl
    GLfloat fa_texcoords[12] = {0.0f, 0.0f,
                               1.0f, 0.0f,
                               1.0f, 1.0f,
                               0.0f, 0.0f,
                               1.0f, 1.0f,
                               0.0f, 1.0f };
    // tl->tr->br->tl->tr->bl
    GLfloat fa_vertex[12] = {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
                            0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};

    glBindMapTextureData(tex_, fa_vertex, fa_texcoords);
    glDrawArrays(GL_TRIANGLES, 0, 6); glCheckError();
    glUnbindMapTextureData();

    glDisable(GL_TEXTURE_2D);
  }

  void Update() {
    Render();
  }

  void SetTexture(const std::vector<unsigned char>& pixels,
                  int format = GL_RGB) {
    assert(format == GL_RGB || format == GL_RGBA);
    tex_pixels_ = pixels;
    tex_format_ = format;
  }

 protected:
  // texture stuffs
  GLuint tex_;
  GLuint tex_format_;  ///< e.x.: RGB, BGR, etc.
  Vec2f  tex_size_;
  std::vector<unsigned char> tex_pixels_;
};
}  // namespace present