// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <Eigen/Core>

namespace present {

typedef Eigen::Matrix<float, 2, 1> Vec2f;
typedef Eigen::Matrix<float, 3, 1> Vec3f;
typedef Eigen::Matrix<float, 4, 1> Vec4f;
typedef Eigen::Matrix<float, 6, 1> Vec6f;
typedef Eigen::Matrix<float, 8, 1> Vec8f;

typedef Eigen::VectorXf VecXf;

typedef Eigen::Matrix<float, 2, 2> Mat2f;
typedef Eigen::Matrix<float, 2, 3> Mat2x3f;
typedef Eigen::Matrix<float, 3, 3> Mat3f;
typedef Eigen::Matrix<float, 3, 4> Mat3x4f;
typedef Eigen::Matrix<float, 4, 4> Mat4f;

struct Rect {
  Rect() = default;
  float x, y, width, height;
};

}  // namespace present