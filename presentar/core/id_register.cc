// Copyright 2014 The PresentAR Authors. All rights reserved.
#include "presentar/core/id_register.h"

namespace present {
static IdType incr_index = 0;

void IdRegister::Reset() {
  incr_index = 0;
}

IdType IdRegister::Register() {
  incr_index += 1;
  return incr_index;
}

const IdType IdRegister::Next() {
  return incr_index + 1;
}

}  // namespace present