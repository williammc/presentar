// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <memory>
#include "presentar/core/datatypes.h"
#include "presentar/core/present_node.h"
#include "presentar/presentar_api.h"

namespace present {

class PRESENTAR_API PresentViewer : public PresentNode {
 public:
  PresentViewer(const PresentViewer&) = delete;
  PresentViewer(uint32_t id = 0, PresentNode* p = nullptr) 
   : PresentNode(id, p) {}

  virtual ~PresentViewer() {}

  virtual void Init() {}

  /// Near far clip for the frustum matrix
  void SetNearFarClip(float z_near, float z_far) {
    z_near_ = z_near;
    z_far_ = z_far;
  }

  virtual Mat4f ProjectionMatrix() const  {
    return proj_mat_;
  }

  virtual void SetProjectionMatrix(const Mat4f& m) {
    proj_mat_ = m;
    SetModelViewProjection(proj_mat_ * view_mat_);
  }

  virtual Mat4f ViewMatrix() const {
    return view_mat_;
  }

  virtual void SetViewMatrix(const Mat4f& m) {
    view_mat_ = m;
    SetModelViewProjection(proj_mat_ * view_mat_);
  }

  int width() { return window_size_[0]; }
  int height() { return window_size_[1]; }
  Vec2f WindowSize() const { return window_size_; }
  void ResizeWindow(const int w, const int h) {
    window_size_ = Vec2f(w, h);
  }

 protected:
  Vec2f window_size_;
  float z_near_, z_far_;
  Mat4f proj_mat_;   ///< projection matrix
  Mat4f view_mat_;   ///< view matrix
};
typedef std::shared_ptr<PresentViewer> PresentViewerPtr;

}  // namespace present