// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <stdint.h>

namespace present {

typedef uint32_t IdType;

struct IdRegister {
  static void Reset();

  /// Register an index value
  static IdType Register();

  /// next index value
  static const IdType Next();
};

}  // namespace present
