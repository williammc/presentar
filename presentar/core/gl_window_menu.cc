// Copyright (c) discliamer
// This is based on Georg Klein's PTAM code
// will be replaced by new code later.

#include "presentar/core/gl_window_menu.h"
#include "signalx/signalx.h"
#include "presentar/util/gl_helpers.h"
#include "presentar/util/gl_draw_helpers.h"

using namespace std;

namespace present {

/// for a click triggers signal with unique command 'code'
static MenuCommandSignal menucmd_signal;

string UncommentString(std::string s) {
  int q = 0;

  for (string::size_type n = 0; n < s.size(); n++) {
    if(s[n] == '"') q = !q;

    if (s[n] == '/' && !q) {
      if(n < s.size() -1 && s[n+1] == '/')
        return s.substr(0, n);
    }
  }

  return s;
};

std::vector<std::string> ChopAndUnquoteString(std::string s) {
  std::vector<std::string> v;
  std::string::size_type pos=0;
  std::string::size_type len = s.length();
  while (true) {
    std::string target;
    char delim;
    // Get rid of leading whitespace:  
    while ((pos < len) && (s[pos] == ' '))
      pos++;
    if (pos == len)
      return v;
    
    // First non-whitespace char...
    if (s[pos] != '\"') {
      delim = ' ';
    } else {
      delim = '\"';
      pos++;
    }
    for (; pos < len; ++pos) {
      char c = s[pos];
      if (c == delim)
          break;
      if (delim == '"' && pos+1 < len && c == '\\') {
        char escaped = s[++pos];
        switch (escaped) {
        case 'n': c = '\n'; break;
        case 'r': c = '\r'; break;
        case 't': c = '\t'; break;
        default: c = escaped; break;
        }
      }
      target+=c;
    }
    v.push_back(target);

    if (delim == '\"')
      pos++;
    }
};

inline void PrintString(const Vec2f& loc, std::string s) {
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glTranslatef(loc[0], loc[1], 0.0);
  glScalef(8, -8, 1);
  glDrawText(s);
  glPopMatrix();
}
GLWindowMenu::GLWindowMenu(std::string name, std::string title,
  int menuitem_w, int menutext_offset) {
  name_ = name;
  title_ = title;
  menuitem_width_ - menuitem_w;
  menutext_offset_ = menutext_offset;

  //Signal().connect<GLWindowMenu, &GLWindowMenu::HandleCommand>(this);

  // GVars3::GUI.RegisterCommand(name_+".AddMenuButton", GUICommandCallBack, this);
  // GVars3::GUI.RegisterCommand(name_+".AddMenuToggle", GUICommandCallBack, this);
  // GVars3::GUI.RegisterCommand(name_+".AddMenuSlider", GUICommandCallBack, this);
  // GVars3::GUI.RegisterCommand(name_+".AddMenuMonitor", GUICommandCallBack, this);
  // GVars3::GUI.RegisterCommand(name_+".ShowMenu", GUICommandCallBack, this);
  // GVars3::GV2.Register(mgvnMenuItemWidth, name_+".MenuItemWidth",
  //                      90, GVars3::HIDDEN | GVars3::SILENT);
  // GVars3::GV2.Register(mgvnMenuTextOffset, name_+".MenuTextOffset",
  //                      20, GVars3::HIDDEN | GVars3::SILENT);
  // GVars3::GV2.Register(mgvnEnabled, name_+".Enabled",
  //                      1, GVars3::HIDDEN | GVars3::SILENT);

  sub_menus_.clear();
  current_submenu_="";
}

GLWindowMenu::~GLWindowMenu() {
  //Signal().disconnect<GLWindowMenu, &GLWindow::GUICommandCallBack>(this);

  // GVars3::GUI.UnRegisterCommand(name_+".AddMenuButton");
  // GVars3::GUI.UnRegisterCommand(name_+".AddMenuToggle");
  // GVars3::GUI.UnRegisterCommand(name_+".AddMenuSlider");
  // GVars3::GUI.UnRegisterCommand(name_+".AddMenuMonitor");
  // GVars3::GUI.UnRegisterCommand(name_+".ShowMenu");
}

void GLWindowMenu::ToggleEnabled() {
  enabled_ = !enabled_;
}

MenuCommandSignal& GLWindowMenu::Signal() {
  return menucmd_signal;
}

void GLWindowMenu::AddSubMenu(MenuItemType type, std::string params) {
  std::vector<std::string> vs = ChopAndUnquoteString(params);

  if (type == MenuItemType::Button) {
    if (vs.size() < 3) {
      printf("? AddSubMenu params: Need 3/4 params: Target Menu, Name, Command , NextMenu=\"\"\n");
      return;
    };
    MenuItem m;
    m.type = MenuItemType::Button;
    m.name = vs[1];
    m.param = UncommentString(vs[2]);
    m.next_menu = (vs.size() > 3) ? (vs[3]) : ("");
    sub_menus_[vs[0]].menu_items.push_back(m);
    return;
  }
#if 0
  if (cmd == name_ + ".AddMenuToggle") {
    if (vs.size() < 3) {
      printf("? GLWindowMenu.AddMenuToggle: Need 3/4 params: Target Menu, Name, gvar_int name , NextMenu=\"\"\n");
      return;
    }
    MenuItem m;
    m.type = Toggle;
    m.name = vs[1];
    GVars3::GV2.Register(m.gvnIntValue, vs[2]);
    m.next_menu = (vs.size() > 3) ? (vs[3]) : ("");
    sub_menus_[vs[0]].menu_items.push_back(m);
    return;
  }

  if (cmd == name_ + ".AddMenuMonitor") {
    if (vs.size() < 3) {
      printf("? GLWindowMenu.AddMenuMonitor: Need 3/4 params: Target Menu, Name, gvar name , NextMenu=\"\"\n");
      return;
    };
    MenuItem m;
    m.type = Monitor;
    m.name = vs[1];
    m.param = vs[2];
    m.next_menu = (vs.size() > 3) ? (vs[3]) : ("");
    sub_menus_[vs[0]].menu_items.push_back(m);
    return;
  }
#endif
}

void GLWindowMenu::ShowSubMenu(std::string name) {
  current_submenu_ = name;
}

void GLWindowMenu::LineBox(int l, int r, int t, int b) {
  glBegin(GL_LINE_STRIP);
  glVertex2i(l, t);
  glVertex2i(l, b);
  glVertex2i(r, b);
  glVertex2i(r, t);
  glVertex2i(l, t);
  glEnd();
}

void GLWindowMenu::FillBox(int l, int r, int t, int b) {
  glBegin(GL_QUADS);
  glVertex2i(l, t);
  glVertex2i(l, b);
  glVertex2i(r, b);
  glVertex2i(r, t);
  glEnd();
}

void GLWindowMenu::Render(int top, int height, int width) {
  if(!enabled_)
    return;

  width_ = width;
  menu_top_ = top;
  menu_height_ = height;

  double alpha = 0.8;
  if (current_submenu_ == "") {  // No Menu selected  - draw little arrow.
    glColor4d(0, 0.5, 0, 0.5);
    FillBox(width_ - 30, width_ - 1, menu_top_, menu_top_ + menu_height_);
    glColor4d(0, 1, 0, 0.5);
    LineBox(width_ - 30, width_ - 1, menu_top_, menu_top_ + menu_height_);
    leftmost_u_ = width_ - 30;
    return;
  }

  SubMenu &m = sub_menus_[current_submenu_];

  leftmost_u_ = width_ - (1 + m.menu_items.size()) * menuitem_width_;
  int base = leftmost_u_;
  for (auto i = m.menu_items.rbegin(); i != m.menu_items.rend(); i++) {
    switch (i->type) {
    case MenuItemType::Button:
      glColor4d(0, 0.5, 0,  alpha);
      FillBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      glColor4d(0, 1, 0,  alpha);
      LineBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      PrintString(Vec2f( base + 3, menu_top_ + menutext_offset_),
                      i->name);
      break;
#if 0
    case Toggle:
      if(*(i->gvnIntValue))
        glColor4d(0,0.5,0.5, alpha);
      else
        glColor4d(0.5,0,0, alpha);
      FillBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      if(*(i->gvnIntValue))
        glColor4d(0,1,0.5, alpha);
      else
        glColor4d(1,0,0, alpha);
      LineBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      PrintString(ImageRef( base + 3, menu_top_ + menutext_offset_),
                      i->name + " " + ((*(i->gvnIntValue))?("On"):("Off")));
      break;

    case Monitor:
      glColor4d(0,0.5,0.5, alpha);
      FillBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      glColor4d(0,1,1, alpha);
      LineBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      PrintString(ImageRef( base + 3, menu_top_ + menutext_offset_),
                      i->name + " " + GVars3::GV2.std::StringValue(i->param, true));
      break;

    case Slider:
    {
      glColor4d(0.0,0.0,0.5, alpha);
      FillBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      glColor4d(0.5,0.0,0.5, alpha);
      double dFrac = (double) (*(i->gvnIntValue) - i->min) / (i->max - i->min);
      if(dFrac<0.0)
        dFrac = 0.0;
      if(dFrac>1.0)
        dFrac = 1.0;
      FillBox(base, (int) (base + dFrac * (menuitem_width_ +1)), menu_top_, menu_top_ + menu_height_);
      glColor4d(0,1,1, alpha);
      LineBox(base, base + menuitem_width_ +1, menu_top_, menu_top_ + menu_height_);
      ostd::stringstream ost;
      ost << i->name << " " << *(i->gvnIntValue);
      PrintString(ImageRef( base + 3, menu_top_ + menutext_offset_),
                      ost.str());
    }
      break;
#endif
    }
    base += menuitem_width_;
  };

  glColor4d(0.5, 0.5, 0, alpha);
  FillBox(width_ - menuitem_width_, width_-1, menu_top_, menu_top_ + menu_height_);
  glColor4d(1,1,0, alpha);
  LineBox(width_ - menuitem_width_, width_-1, menu_top_, menu_top_ + menu_height_);
  Vec2f ir( width_ - menuitem_width_ + 5, menu_top_ + menutext_offset_);
  if(current_submenu_ == "Root")
    PrintString(ir, title_+":");
  else
    PrintString(ir, current_submenu_+":");
}

bool GLWindowMenu::HandleClick(int mouse_button, int state, int x, int y) {
  if (!enabled_)
    return false;

  if ((y < menu_top_) || (y > menu_top_ + menu_height_))
    return false;
  if (x < leftmost_u_)
    return false;

  // if no menu displayed, then must display root menu!
  if (current_submenu_ == "") {
    current_submenu_ = "Root";
    return true;
  };

  // Figure out which button was pressed:
  int button_number = (width_ - x) / menuitem_width_;
  if (button_number > (int)(sub_menus_[current_submenu_].menu_items.size()))
    button_number = 0;

  if (button_number == 0) {  // Clicked on menu name .. . go to root.
    if (current_submenu_ == "Root")
      current_submenu_ = "";
    else
      current_submenu_ = "Root";
    return true;
  };

  MenuItem SelectedItem  = sub_menus_[current_submenu_].menu_items[button_number-1];
  current_submenu_ = SelectedItem.next_menu;
  switch (SelectedItem.type) {
  case MenuItemType::Button:
    Signal()(SelectedItem.param, "");  // emit command signal
    break;
#if 0
  case Toggle:
    *(SelectedItem.gvnIntValue)^=1;
    break;
  case Slider: {
    if(mouse_button == GLWindow::BUTTON_WHEEL_UP) {
      *(SelectedItem.gvnIntValue)+=1;
      if(*(SelectedItem.gvnIntValue) > SelectedItem.max)
        *(SelectedItem.gvnIntValue) = SelectedItem.max;
    } else if(mouse_button == GLWindow::BUTTON_WHEEL_DOWN) {
      *(SelectedItem.gvnIntValue)-=1;
      if(*(SelectedItem.gvnIntValue) < SelectedItem.min)
        *(SelectedItem.gvnIntValue) = SelectedItem.min;
    } else {
      int pos = menuitem_width_ - ((width_ - x) % menuitem_width_);
      double dFrac = (double) pos / menuitem_width_;
      *(SelectedItem.gvnIntValue) = (int)(dFrac * (1.0 + SelectedItem.max - SelectedItem.min)) + SelectedItem.min;
    };
  }
    break;
  case Monitor:
    break;
#endif
  };
  return true;
}
}  // namespace present