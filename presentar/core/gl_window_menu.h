// Copyright (c) discliamer
// This is based on Georg Klein's PTAM code
// will be replaced by new code later.

#pragma once

// A simple menu system for GLWindow, remade of George Klein's implementation
// N.b. each GLWindowMenu class internally contains sub-menus
#include <memory>
#include <unordered_map>
#include <vector>
#include "signalx/signalx.h"
#include "presentar/core/common_macros.h"
#include "presentar/core/gl_window.h"
#include "presentar/presentar_api.h"

namespace present {
typedef sigx::Signal<void(std::string, std::string)> MenuCommandSignal;
FORWARD_DECLARE_CLASS_SHARED_PTR(GLWindowMenu)

std::string PRESENTAR_API UncommentString(std::string s);
std::vector<std::string> PRESENTAR_API ChopAndUnquoteString(std::string s);

class PRESENTAR_API GLWindowMenu {
 public:
  GLWindowMenu(std::string name, std::string title, 
               int menuitem_w = 90, int menutext_offset = 20);
  ~GLWindowMenu();
  void ToggleEnabled();
  void Render(int nTop, int nHeight, int nWidth);
  void FillBox(int l, int r, int t, int b);
  void LineBox(int l, int r, int t, int b);

  static MenuCommandSignal& Signal();
  enum struct MenuItemType : unsigned { Button = 0, Toggle, Monitor, Slider };
  void AddSubMenu(MenuItemType type, std::string params);
  void ShowSubMenu(std::string name);
  void HandleCommand(std::string command, std::string params);
  
  bool HandleClick(int button, int state, int x, int y);

protected:
  struct MenuItem {
    MenuItemType type;
    std::string name;
    std::string param;
    std::string next_menu;
    int min;
    int max;
  };

  struct SubMenu {
    std::vector<MenuItem> menu_items;
  };
  
  bool enabled_;
  std::unordered_map<std::string, SubMenu> sub_menus_;
  std::string current_submenu_;
  std::string name_;
  std::string title_;

  int width_;
  int menu_top_;
  int menu_height_;
  int text_offset_;

  int menuitem_width_;
  int menutext_offset_;

  int leftmost_u_;

};
}  // namespace present