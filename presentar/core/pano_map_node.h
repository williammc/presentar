// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include "slick/scene/poli_camera.h"
#include "slick/scene/cylinder_camera.h"

#include "presentar/core/datatypes.h"
#include "presentar/core/fbo_node.h"
#include "presentar/core/opengl.h"
#include "presentar/presentar_api.h"


#include <vector>

namespace present {
typedef slick::PoliCamera<float> PoliCam;

// ARWidget for AR visualization
struct PanoMapNode : public FboNode {
  PanoMapNode() = delete;
  PanoMapNode(const PanoMapNode&) = delete;
  PanoMapNode(Rect pano_rect, const VecXf& cam_params, const float h,
              uint32_t id = 0, PresentNode* parent = nullptr);
  ~PanoMapNode();

  virtual void Init();
  virtual void Render();

  void SaveSnapshot(std::string filename,
                    std::string withmeas_filename) const;

  void reset_panorama() {
    need_reset_panorama_ = true;
  }

  virtual void set_camera_pose(const Mat4f& pose) {
    world2cam_pose_ = pose;
  }

  virtual bool set_frame_pixels(int width, int height, int depth,
                                unsigned char* data) {
    int nbytes = width*height*depth;
    if (videoframe_pixels_.size() != nbytes)
      return false;
    videoframe_pixels_.resize(nbytes);
    memcpy(&videoframe_pixels_.front(), data, nbytes);
    update_panorama_ = true;
    return true;
  }

  virtual bool set_frame_pixels(std::vector<unsigned char>& pixels) {
    if (videoframe_pixels_.size() != pixels.size()) {
      return false;
    }
    videoframe_pixels_ = pixels;  // hopefully it's efficiently assigned ;)
    update_panorama_ = true;
    return true;
  }

  void set_update_panorama(bool update = true) {
    update_panorama_ = update;
  }

  void set_highlight_color(const Eigen::Vector3f& color) {
    highlight_color_ = color;
  }

  slick::Cylinder<float>& cylinder() {
    return cylinder_;
  }

  // convert coordinates from imageplane to current widget window coordinates
  Vec2f convert_coordinate(const Vec2f& v) {
    return Vec2f(v[0] * video_size_[0] / buffer_size_[0],
                 v[1] * video_size_[1] / buffer_size_[1]);
  }

  Vec2f panorama_size() { return buffer_size_; }

  GLuint panorama_buffer() { return buffer_; }

  void ResizeWindow(Rect rect);

protected:

  void Configure(float height_angle = 120.);

  Vec2f MapCurrentPixelToPanorama(
    Vec2f const& v2_implane, const Mat3f& w2c) const;

  // map 3D point to panorama 1-unit coordinate
  Vec2f Map3DPointToUnitPanorama(const Vec3f& v3_worldpoint) const;

  // map 3D point to panorama coordinates
  Vec2f Map3DPointToPanorama(const Vec3f& v3_worldpoint) const;

  // upload a full texture to panorama
  //void UploadTextureToPanorama(cv::Mat& texture);
  
  void HighlightCurrentFrame();

  void UploadVideoFrame();
  bool GenerateFrameBuffers();
  void MapPanoramaTexture2SystemFBO();

  // Properties ===============================================================
  Rect pano_rect_;  ///< pano map position in the main window
  PoliCam camera_;
  std::vector<unsigned char> videoframe_pixels_;
  Mat4f  world2cam_pose_;
  Vec3f highlight_color_;

  bool update_panorama_;
  bool need_reset_panorama_;
  bool save_snapshot_;
  std::string snapshot_filename_;
  slick::Cylinder<float> cylinder_;  // for panorama texture mapping

  std::vector<Vec2f> top_lines_vertices_;
  std::vector<Vec2f> bottom_lines_vertices_;
  std::vector<Vec2f> left_lines_vertices_;
  std::vector<Vec2f> right_lines_vertices_;

  // Texture stuffs for videoframe
  GLuint frame_tex_;
  Vec2f video_size_;

  bool texture_initialized_;
};  // class PanoWidget
}  // namespace present
