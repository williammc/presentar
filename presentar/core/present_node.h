// Copyright 2014 The PresentAR Authors. All rights reserved.
#pragma once
#include <stdint.h>
#include <vector>
#include "presentar/core/common_macros.h"
#include "presentar/core/datatypes.h"
#include "presentar/presentar_api.h"

namespace present {

FORWARD_DECLARE_CLASS_SHARED_PTR(PresentNode)
FORWARD_DECLARE_STRUCT_SHARED_PTR(RenderOption)

/// Represent a 3D bounding box
struct PRESENTAR_API BBox {
  BBox() {
    min_x = std::numeric_limits<float>::max();
    min_y = std::numeric_limits<float>::max();
    min_z = std::numeric_limits<float>::max();
    max_x = std::numeric_limits<float>::min();
    max_y = std::numeric_limits<float>::min();
    max_z = std::numeric_limits<float>::min();
  }
  float min_x, min_y, min_z, max_x, max_y, max_z;
};

struct PRESENTAR_API RenderOption {
  RenderOption() = default;
  virtual ~RenderOption() {}

  virtual unsigned Option() const = 0;
  virtual void SetOption(const unsigned opt) = 0;
};

/// Represent a scene node for rendering
class PRESENTAR_API PresentNode {
 public:
  PresentNode(const PresentNode&) = delete;
  PresentNode(uint32_t id = 0, PresentNode *parent = nullptr)
    : id_(id), parent_(parent), initialized_(false) {
  }

  virtual ~PresentNode() {}
  virtual void Init() = 0;
  virtual void Render() {}

  virtual void Update() {
    DoBeforeRender();
    Render();
    DoAfterRender();

    for (auto child : children_) child->Update();
  }

  virtual void Sync() {
    for (auto child : children_) child->Sync();
  }

  virtual void ComputeBBox() {}
  BBox bbox() const { return bbox_; }

  virtual bool IsRoot() const { return parent_ == nullptr; }

  uint32_t id() const {
    return id_;
  }

  void SetId(uint32_t id) {
    id_ = id;
  }

  PresentNode* Parent() const {
    return parent_;
  }

  void SetParent(PresentNode* parent) {
    parent_ = parent;
  }

  void AddChild(PresentNodePtr obj) {
    children_.push_back(obj);
    obj->SetParent(this);
  }

  void RemoveChild(PresentNode* obj) {
    std::vector<PresentNodePtr> new_ch_;
    for (auto child : children_) {
      if (child.get() != obj) {
        new_ch_.push_back(child);
      }
    }
    children_.swap(new_ch_);
  }

  void ClearChildren() {
    children_.clear();
  }

  PresentNode* GetChildById(const uint32_t id) const {
    for (auto child : children_) {
      if (child->id() == id) return child.get();
    }
    return nullptr;
  }

  RenderOptionPtr RenderOption() const{ return render_opt_; }

  void SetRenderOption(present::RenderOption* vo) {
    if(render_opt_) render_opt_->SetOption(vo->Option());
    for (auto child : children_) child->SetRenderOption(vo);
  }

  void SetRenderOption(RenderOptionPtr vo) {
    render_opt_ = vo;
    for (auto child : children_) child->SetRenderOption(vo);
  }

  const time_t& LastDataUpdate() const {
    return lst_data_up_;
  }

  void SetLastDataUpdate(const time_t t) {
    lst_data_up_ = t;
  }

  Mat4f ModelViewProjection() const {
    return mvp_matrix_;
  }

  void SetModelViewProjection(const Mat4f& mvp) {
    mvp_matrix_ = mvp;
  }

 protected:
  void DoBeforeRender() {
    Init();
  }

  void DoAfterRender() {
  }

  // Properties ----------------------------------------------------------------
  uint32_t id_;       ///< each node has a unique id
  bool initialized_;  ///< indicate this item is prober initialized

  RenderOptionPtr render_opt_;

  PresentNode* parent_;
  std::vector<PresentNodePtr> children_;

  time_t lst_data_up_;  ///< latest update time for this object's data
  BBox bbox_;

  Mat4f mvp_matrix_;  ///< model-view-projection matrix
};

// Commonly used node types ====================================================
/// A group node
class PRESENTAR_API PresentGroup : public PresentNode {
 public:
  PresentGroup(const PresentGroup&) = delete;
  PresentGroup(uint32_t id, PresentNode* parent)
   : PresentNode(id, parent) {}

  virtual void Init() {}
};

/// A transformation node
class PRESENTAR_API PresentTransform : public PresentNode {
 public:
  PresentTransform(const PresentTransform&) = delete;
  
  PresentTransform(const Mat4f& trans, 
                   uint32_t id, 
                   PresentNode* parent)
   : PresentNode(id, parent) {}

  virtual void Init() {}
  void SetModelViewProjection(const Mat4f& mvp) {
    mvp_matrix_ = mvp;
    for (auto& child : children_) child->SetModelViewProjection(mvp_matrix_ * model_mat_);
  }

  Mat4f ModelMatrix() const { return model_mat_; }
  void SetModelMatrix(const Mat4f& m) { model_mat_ = m; }

 protected:
  Mat4f model_mat_;
};
}  // namespace present