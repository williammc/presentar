cmaker_print_status("Setup required dependencies for PresentAR tracking library")

##==============================================================================

# OpenGL
find_package(OpenGL REQUIRED)
message(STATUS "Found OpenGL ? ${OPENGL_FOUND}")
if(OPENGL_FOUND)
  message(STATUS "OpenGL INCLUDE: ${OPENGL_INCLUDE_DIR}")
  include_directories(${OPENGL_INCLUDE_DIR})
  list(APPEND PresentAR_EXTERNAL_LIBS ${OPENGL_LIBRARIES})
endif()

# GLUT
find_package(GLUT REQUIRED)
if(GLUT_FOUND)
  include_directories(${GLUT_INCLUDE_DIR})
  list(APPEND PresentAR_EXTERNAL_LIBS ${GLUT_LIBRARIES})
  if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
    get_filename_component(GLUT_BINARY_DIR "${GLUT_INCLUDE_DIR}/../lib/x64" ABSOLUTE)
  else()
    get_filename_component(GLUT_BINARY_DIR "${GLUT_INCLUDE_DIR}/../lib/x86" ABSOLUTE)
  endif()
  list(APPEND THE_DEPEDENCIES_BINARY_PATHS ${GLUT_BINARY_DIR})
endif()

# GLEW
if(WIN32)
  # GLEW
  find_package(GLEW REQUIRED)
  if(GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIR})
    list(APPEND PresentAR_EXTERNAL_LIBS ${GLEW_LIBRARIES})
    if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
      get_filename_component(GLUT_BINARY_DIR "${GLUT_INCLUDE_DIR}/../bin/Release/x64" ABSOLUTE)
    else()
      get_filename_component(GLEW_BINARY_DIR "${GLEW_INCLUDE_DIR}/../bin/Release/Win32" ABSOLUTE)
    endif()
    list(APPEND THE_DEPEDENCIES_BINARY_PATHS ${GLEW_BINARY_DIR})
  endif()
endif()